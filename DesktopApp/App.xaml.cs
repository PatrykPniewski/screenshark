﻿using ScreenShark.Properties;
using ScreenShark.src.Enums;
using ScreenShark.src.Extensions;
using ScreenShark.src.Hotkey;
using ScreenShark.src.ViewModels;
using ScreenShark.src.Views;
using ScreenShark.src.WindowManagerNS;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Threading;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using Windows.Win32.UI.Input.KeyboardAndMouse;

namespace ScreenShark {
    /// <summary>
    /// Logika interakcji dla klasy App.xaml
    /// </summary>
    public partial class App : System.Windows.Application {
        private readonly NotifyIcon NotifyIcon;
        public App() {
            InitializeComponent();
            bool createdNew = true;
            Mutex mutex = new Mutex(true, Settings.Default.MutexName, out createdNew);
            if (!createdNew) {
                this.Shutdown();
                return;
            }

            if (Settings.Default.Language != SupportedLanguages.Default)
                Localization.Resources.Culture = new CultureInfo((int) Settings.Default.Language);

            string TempFolder = Path.GetTempPath() + "\\ScreenShark";

            // Initialize default settings
            if (Settings.Default.TempFolder == string.Empty)
                Settings.Default.TempFolder = TempFolder;

            if (Settings.Default.SaveFilePath == string.Empty)
                Settings.Default.SaveFilePath = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures);

            if (Settings.Default.LastSelection == string.Empty) {
                Rectangle virtualScreen = SystemInformation.VirtualScreen;
                RectangleGeometry screenGeometry = new(new Rect(virtualScreen.X, virtualScreen.Y, virtualScreen.Width, virtualScreen.Height));
                Settings.Default.LastSelection = PathGeometry.CreateFromGeometry(screenGeometry).ToString(CultureInfo.InvariantCulture);
            }

            // Initialize tray icon
            NotifyIcon = new NotifyIcon {
                Visible = true,
                Icon = ScreenShark.Properties.Resources.Icon
            };
            NotifyIcon.DoubleClick += OpenSettingsWindow;
            NotifyIcon.ContextMenuStrip = new ContextMenuStrip();
            NotifyIcon.Text = "Screenshark";

            // Menu items
            List<ToolStripMenuItem> menuItems = new List<ToolStripMenuItem> {
                new ToolStripMenuItem("Settings", null, OpenSettingsWindow),
                new ToolStripMenuItem("Close", null, Close)
            };

            // Add menu items
            NotifyIcon.ContextMenuStrip.Items.AddRange(menuItems.ToArray());

            // Set up hotkey
            HotKey key = new(0, Key.PrintScreen, (hotkey) => {
                WindowManager.GetWindow<SelectScreen>().Show();
            });
            HotKey key2 = new(HOT_KEY_MODIFIERS.MOD_SHIFT, Key.PrintScreen, (hotkey) => {
                ((EditScreenshotViewModel) WindowManager.GetWindow<EditScreenshot>().DataContext).RepeatScreenshot();
            });

            // If Directory exits clear it
            if (Directory.Exists(TempFolder))
                Directory.Delete(TempFolder, true);

            Directory.CreateDirectory(TempFolder);

            // Save settings
            Settings.Default.Save();
        }

        private void OnExit(object sender, ExitEventArgs e) {
            NotifyIcon?.Dispose();
        }

        private void Close(object sender, EventArgs e) {
            this.Shutdown();
        }

        private void OpenSettingsWindow(object sender, EventArgs e) {
            SettingsWindow settingsWindow = WindowManager.GetWindow<SettingsWindow>();
            settingsWindow.Show();
            settingsWindow.BringToFront();
        }
    }
}
