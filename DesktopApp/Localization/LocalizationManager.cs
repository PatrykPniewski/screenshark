﻿using ScreenShark.src.Interfaces;
using System.Collections.Generic;

namespace ScreenShark.Localization {
    public static class LocalizationManager {
        static List<ILocalizationExtension> ResxExtensions = new List<ILocalizationExtension>();

        public static void Register(ILocalizationExtension ext) {
            ResxExtensions.Add(ext);
        }

        public static void UpdateAll() {
            foreach (ILocalizationExtension ext in ResxExtensions) {
                ext.Update();
            }
        }
    }
}
