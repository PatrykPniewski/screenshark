﻿using CommunityToolkit.Mvvm.ComponentModel;
using System;

namespace ScreenShark.src.PageManagerNS {
    public partial class PageManager : ObservableObject {
        public event EventHandler<int> PageChanged;
        public event EventHandler<int> MaxPagesChanged;
        [ObservableProperty]
        private int currentPage = -1;
        [ObservableProperty]
        private int maxPages = 0;

        public PageManager(int max_pages) {
            this.MaxPages = max_pages;
        }

        partial void OnCurrentPageChanged(int oldValue, int newValue) {
            PageChanged?.Invoke(this, CurrentPage);
        }

        partial void OnMaxPagesChanged(int oldValue, int newValue) {
            MaxPagesChanged?.Invoke(this, MaxPages);
        }

        public void NextPage() {
            if (CurrentPage == MaxPages)
                CurrentPage = 1;
            else
                CurrentPage++;
        }

        public void PreviousPage() {
            if (CurrentPage == 1)
                CurrentPage = MaxPages;
            else
                CurrentPage--;
        }
    }
}