﻿using ScreenShark.src.Extensions;
using ScreenShark.src.Helpers;
using System.Drawing;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace ScreenShark.src.PrintScreenNS {
    public static class PrintScreen {
        public static Bitmap Rectangle(int left, int top, int width, int height) {
            Bitmap bmp = new Bitmap(width, height);
            using (Graphics g = Graphics.FromImage(bmp)) {
                g.CopyFromScreen(left, top, 0, 0, bmp.Size);
            }
            return bmp;
        }

        public static Bitmap FullScreen() {
            int left = SystemInformation.VirtualScreen.Left;
            int top = SystemInformation.VirtualScreen.Top;
            int width = SystemInformation.VirtualScreen.Width;
            int height = SystemInformation.VirtualScreen.Height;

            return Rectangle(left, top, width, height);
        }

        public static BitmapSource FromGeometry(Geometry geometry) {
            return FromGeometry(FullScreen().ToBitmapImage(), geometry);
        }

        public static BitmapSource FromGeometry(BitmapSource source, Geometry geometry) {
            DrawingVisual visual = new();
            using (DrawingContext r = visual.RenderOpen()) {
                r.PushClip(geometry);
                r.DrawImage(source, new Rect(0, 0, source.PixelWidth, source.PixelHeight));
            }
            RenderTargetBitmap renderTarget = RenderTargetBitmapHelpers.GetRenderTargetBitmap(source);
            renderTarget.Render(visual);

            Rect bounds = geometry.Bounds;
            if (bounds.Width <= 0 || bounds.Height <= 0) return null;

            return new CroppedBitmap(renderTarget, new Int32Rect((int) bounds.X, (int) bounds.Y, (int) bounds.Width, (int) bounds.Height));
        }
    }
}
