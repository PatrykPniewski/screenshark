﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using ScreenShark.Properties;
using ScreenShark.src.Extensions;
using ScreenShark.src.TempFileNS;
using System.Collections.Generic;
using System.IO;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace ScreenShark.src.ViewModels {
    public partial class UploadWindowViewModel : ObservableObject {
        [ObservableProperty]
        private string _FileName;
        readonly List<BitmapSource> Frames;
        readonly List<TempFile> Files = new();

        public UploadWindowViewModel(List<BitmapSource> frames) {
            this.Frames = frames;
        }

        private void Rectangle_MouseLeftButtonDown(object sender, MouseButtonEventArgs e) {
            CleanUpFiles();

            string ext = Settings.Default.UploadFileExt.ToString();
            string filename;
            for (int i = 0; i < Frames.Count; i++) {
                if (i == 0)
                    filename = $"{FileName}.{ext}";
                else
                    filename = $"{FileName}{i}.{ext}";

                BitmapEncoder enc = BitmapEncoderExtensions.GetEncoder(ext);
                enc.Frames.Add(BitmapFrame.Create(Frames[i]));

                TempFile tmp = new TempFile();
                using (FileStream fs = tmp.Create(filename)) {
                    enc.Save(fs);
                }
                Files.Add(tmp);
            }
            Settings.Default.Save();
        }

        [RelayCommand]
        private void CleanUpFiles() {
            foreach (TempFile file in Files)
                file.Dispose();
            Files.Clear();
        }
    }
}
