﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using ScreenShark.Properties;
using ScreenShark.src.Enums;
using ScreenShark.src.Extensions;
using ScreenShark.src.Models;
using ScreenShark.src.PageManagerNS;
using ScreenShark.src.PrintScreenNS;
using ScreenShark.src.Views;
using ScreenShark.src.WindowManagerNS;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ScreenShark.src.ViewModels {
    public partial class EditScreenshotViewModel : ObservableObject {
        [ObservableProperty]
        private EditableImage currentImage = null;

        [ObservableProperty]
        private Point mousePosition = new(0, 0);

        [ObservableProperty]
        private PageManager pageManager = new(max_pages: 1);

        [ObservableProperty]
        private int strokeThickness = Settings.Default.LineThickness;

        [ObservableProperty]
        private Brush brush = Brushes.Red;

        [ObservableProperty]
        private ObservableCollection<Brush> availableBrushes = new() {
            Brushes.Black,
            Brushes.White,
            Brushes.Red,
            Brushes.Green,
            Brushes.Orange,
            Brushes.Blue,
            Brushes.Yellow,
            Brushes.Brown,
            Brushes.Pink,
            Brushes.Purple
        };

        [ObservableProperty]
        private Visibility pointerVisibility = Visibility.Hidden;

        [ObservableProperty]
        [NotifyCanExecuteChangedFor(nameof(UndoCommand), new string[] { nameof(RedoCommand) })]
        private bool canUndoRedo = true;

        private readonly EditableImageCollection Images = new();

        public EditScreenshotViewModel() {
            PageManager.PageChanged += PageChanged;
        }

        private void PageChanged(object sender, int page) {
            CurrentImage = Images[page - 1];
        }

        public void AddFrame(BitmapSource image) {
            Images.Add(new EditableImage(image));
            PageManager.MaxPages = Images.Count;
            PageManager.CurrentPage = Images.Count;
        }

        private List<BitmapSource> GetScreenshots(FileSaveMode mode) {
            List<BitmapSource> screenshots = new();
            switch (mode) {
                default:
                case FileSaveMode.CombineVertically:
                    screenshots.Add(Images.CombineScreenshotsVertically());
                    break;
                case FileSaveMode.CombineHorizontally:
                    screenshots.Add(Images.CombineScreenshotsHorizontally());
                    break;
                case FileSaveMode.OneFrame:
                    screenshots.Add(CurrentImage.Render());
                    break;
                case FileSaveMode.MultipleFiles:
                    foreach (EditableImage image in Images)
                        screenshots.Add(image.Render());
                    break;
            }
            return screenshots;
        }

        [RelayCommand]
        private void ShowSelectScreenshotScreen() {
            WindowManager.GetWindow<SelectScreen>().Show();
        }

        [RelayCommand]
        private void AddPolyline() {
            // Polyline with only 1 point doesn't show
            CurrentImage.AddPolyline(new Polyline {
                Points = new() { new Point(MousePosition.X, MousePosition.Y), new Point(MousePosition.X, MousePosition.Y) },
                StrokeStartLineCap = PenLineCap.Round,
                StrokeLineJoin = PenLineJoin.Round,
                StrokeEndLineCap = PenLineCap.Round,
                StrokeThickness = StrokeThickness,
                Stroke = Brush
            });
        }

        [RelayCommand]
        private void AddPolylineIfLMBDown(MouseEventArgs e) {
            if (e.LeftButton == MouseButtonState.Pressed)
                AddPolyline();
        }

        [RelayCommand]
        private void RemoveFrame() {
            if (PageManager.MaxPages == 1)
                // TODO: Close window instead of not doing anything?
                return;

            Images.RemoveAt(PageManager.CurrentPage - 1);
            PageManager.MaxPages--;
            PageManager.PreviousPage();
        }

        [RelayCommand]
        private void AddPointToLastPolyline(MouseEventArgs e) {
            if (e.LeftButton == MouseButtonState.Pressed && CurrentImage.Polylines.Count > 0) {
                CurrentImage.AddPointToLastPolyline(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        [RelayCommand]
        private void DisableUndoRedo() {
            CanUndoRedo = false;
        }

        [RelayCommand]
        private void EnableUndoRedo() {
            CanUndoRedo = true;
        }

        [RelayCommand]
        private void ShowPointer() {
            PointerVisibility = Visibility.Visible;
        }

        [RelayCommand]
        private void HidePointer() {
            PointerVisibility = Visibility.Hidden;
        }

        [RelayCommand]
        private void NextPage() {
            PageManager.NextPage();
        }

        [RelayCommand]
        private void PreviousPage() {
            PageManager.PreviousPage();
        }

        [RelayCommand]
        private void IncreaseLineThickness() {
            StrokeThickness++;
        }

        [RelayCommand]
        private void DecreaseLineThickness() {
            if (StrokeThickness == 1)
                return;

            StrokeThickness--;
        }

        [RelayCommand(CanExecute = nameof(CanUndoRedo))]
        private void Undo() {
            CurrentImage.Undo();
        }

        [RelayCommand(CanExecute = nameof(CanUndoRedo))]
        private void Redo() {
            CurrentImage.Redo();
        }

        [RelayCommand]
        private void CopyToClipboard() {
            if (Settings.Default.CopyAllFramesToClipboard) {
                switch (Settings.Default.FileSaveMode) {
                    case FileSaveMode.CombineHorizontally:
                        Clipboard.SetImage(Images.CombineScreenshotsHorizontally());
                        break;
                    default:
                        Clipboard.SetImage(Images.CombineScreenshotsVertically());
                        break;
                }
                return;
            }

            Clipboard.SetImage(CurrentImage.Render());
        }

        [RelayCommand]
        private void Save() {
            System.Windows.Forms.SaveFileDialog saveFileDialog = new System.Windows.Forms.SaveFileDialog {
                Filter = "Png (*.png)|*.png|Gif (*.gif)|*.gif|Tiff (*.tiff)|*.tiff|Jpeg (*.jpeg)|*.jpeg|Bmp (*.bmp)|*.bmp|Wmp (*.wmp)|*.wmp",
                FilterIndex = 0,
                InitialDirectory = Settings.Default.SaveFilePath,
                AddExtension = true,
                CreatePrompt = false,
                OverwritePrompt = true
            };
            if (saveFileDialog.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                return;

            string ext = System.IO.Path.GetExtension(saveFileDialog.FileName);
            string fileName = System.IO.Path.GetFileNameWithoutExtension(saveFileDialog.FileName);
            string folderPath = System.IO.Path.GetDirectoryName(saveFileDialog.FileName);

            Settings.Default.SaveFilePath = folderPath;
            Settings.Default.Save();

            BitmapEncoder encoder = BitmapEncoderExtensions.GetEncoder(ext);

            // TODO: Create setting for this
            // if (encoder.CodecInfo.SupportsMultipleFrames) {
            //     encoder.Frames = GetScreenshots();
            //     encoder.SaveEncoder(saveFileDialog.FileName);
            //     return;
            // }

            // Get screnshots
            List<BitmapSource> screenshots = GetScreenshots(Settings.Default.FileSaveMode);

            encoder.Frames.Add(BitmapFrame.Create(screenshots[0]));
            encoder.SaveEncoder(saveFileDialog.FileName);
            for (int i = 1; i < screenshots.Count; i++) {
                encoder.Frames[0] = BitmapFrame.Create(screenshots[i]);
                encoder.SaveEncoder($"{folderPath}\\{fileName}{i}{ext}");
            }

            saveFileDialog.Dispose();
        }

        [RelayCommand]
        private void SelectBrush(Brush brush) {
            Brush = brush;
        }

        [RelayCommand]
        private void ChangeAvailableBrush(Brush brush) {
            int index = AvailableBrushes.IndexOf(brush);
            // TODO: Add notification instead of doing nothing
            if (index < 0) return;

            // TODO: Custom color picker
            System.Windows.Forms.ColorDialog colorDialog = new System.Windows.Forms.ColorDialog();
            if (colorDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
                var selectedColor = colorDialog.Color;
                Color color = Color.FromArgb(selectedColor.A, selectedColor.R, selectedColor.G, selectedColor.B);
                AvailableBrushes[index] = new SolidColorBrush(color);
            }

            colorDialog.Dispose();
        }

        [RelayCommand]
        private void OpenSettings() {
            SettingsWindow settingsWindow = WindowManager.GetWindow<SettingsWindow>();
            settingsWindow.Show();
            settingsWindow.BringToFront();
        }

        [RelayCommand]
        private void UploadScreenshot() {
            List<BitmapSource> screenshots = GetScreenshots(Settings.Default.FileSaveMode);
            new UploadWindow(screenshots).ShowDialog();
        }

        [RelayCommand]
        private void ClearUserEditsForCurrentPage() {
            CurrentImage.ClearPolylines();
        }

        [RelayCommand]
        private void MouseSideButtonsNavigation(MouseButtonEventArgs e) {
            switch (e.ChangedButton) {
                case MouseButton.XButton1:
                    PreviousPage();
                    break;
                case MouseButton.XButton2:
                    NextPage();
                    break;
            }
        }

        [RelayCommand]
        public void RepeatScreenshot() {
            BitmapSource source = PrintScreen.FromGeometry(Geometry.Parse(Settings.Default.LastSelection));

            // TODO Send notification to user
            if (source == null) return;

            this.AddFrame(source);
        }
    }
}
