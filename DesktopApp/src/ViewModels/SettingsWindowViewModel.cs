﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using ScreenShark.Properties;
using ScreenShark.src.Enums;
using System.Globalization;
using Windows.Win32;

namespace ScreenShark.src.ViewModels {
    public partial class SettingsWindowViewModel : ObservableObject {
        [ObservableProperty]
        private SupportedLanguages _SupportedLanguage = Settings.Default.Language;

        public SettingsWindowViewModel() { }

        partial void OnSupportedLanguageChanged(SupportedLanguages oldValue, SupportedLanguages newValue) {
            uint choosenLang = (uint) newValue;
            if (newValue == SupportedLanguages.Default)
                choosenLang = PInvoke.GetUserDefaultLCID();

            Localization.Resources.Culture = new CultureInfo((int) choosenLang);
            Localization.LocalizationManager.UpdateAll();
            Settings.Default.Language = newValue;
        }

        [RelayCommand]
        private void SaveSettings() {
            Settings.Default.Save();
        }
    }
}
