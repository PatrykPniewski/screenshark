﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using ScreenShark.Properties;
using ScreenShark.src.Extensions;
using ScreenShark.src.PrintScreenNS;
using ScreenShark.src.SelectionModes;
using ScreenShark.src.Views;
using ScreenShark.src.WindowManagerNS;
using System;
using System.Globalization;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace ScreenShark.src.ViewModels {
    public partial class SelectScreenViewModel : ObservableObject {
        [ObservableProperty]
        private Point mousePosition = new(0, 0);

        [ObservableProperty]
        private SelectionMode selectionMode;

        [ObservableProperty]
        private BitmapSource screenshot = PrintScreen.FullScreen().ToBitmapImage();

        [ObservableProperty]
        private Geometry reverseClip;

        [ObservableProperty]
        private Rect imageRect;

        [ObservableProperty]
        private SelectionMode[] selectionModes = new SelectionMode[] { new RectangleSelection(), new PolylineSelection(), new WindowSelection() };

        private RectangleGeometry imageGeometry;

        public SelectScreenViewModel() {
            int lastUsedModeIndex = Settings.Default.LastSelectionModeIndex;
            if (lastUsedModeIndex == -1)
                lastUsedModeIndex = 0;
            SelectionMode = SelectionModes[lastUsedModeIndex];
            ImageRect = new Rect(0, 0, Screenshot.Width, Screenshot.Height);
            imageGeometry = new RectangleGeometry(ImageRect);
        }

        [RelayCommand]
        private void BeginSelection() {
            SelectionMode.Start(MousePosition);
            UpdateReverseClip();
        }

        [RelayCommand]
        private void UpdateSelection() {
            SelectionMode.Update(MousePosition);
            UpdateReverseClip();
        }

        [RelayCommand]
        private void FinishSelection() {
            if (!SelectionMode.IsRunning) return;

            SelectionMode.Stop(MousePosition);
            UpdateReverseClip();

            BitmapSource selectedRegion = PrintScreen.FromGeometry(Screenshot, SelectionMode.Clip);
            if (selectedRegion == null) return;

            OpenEditWindow(selectedRegion);
        }

        [RelayCommand]
        private void SetSelectionMode(SelectionMode selectionMode) {
            SelectionMode = selectionMode;
            Settings.Default.LastSelectionModeIndex = Array.FindIndex(SelectionModes, mode => mode == selectionMode);
            Settings.Default.Save();
        }

        [RelayCommand]
        private void OpenEditWindow(BitmapSource bitmap) {
            if (SelectionMode.Clip == null)
                Settings.Default.LastSelection = PathGeometry.CreateFromGeometry(imageGeometry).ToString(CultureInfo.InvariantCulture);
            else
                Settings.Default.LastSelection = PathGeometry.CreateFromGeometry(SelectionMode.Clip).ToString(CultureInfo.InvariantCulture);
            Settings.Default.Save();

            EditScreenshot editScreenshotWindow = WindowManager.GetWindow<EditScreenshot>();
            EditScreenshotViewModel viewModel = (EditScreenshotViewModel) editScreenshotWindow.DataContext;
            viewModel.AddFrame(bitmap);
            editScreenshotWindow.Show();
            CloseWindow();
        }

        [RelayCommand]
        private void ClearClip() {
            ReverseClip = null;
            SelectionMode.Stop(MousePosition);
            SelectionMode.Clip = null;
        }

        void CloseWindow() {
            WindowManager.GetWindow<SelectScreen>().Close();
        }

        private void UpdateReverseClip() {
            if (SelectionMode.Clip == null) return;
            ReverseClip = Geometry.Combine(imageGeometry, SelectionMode.Clip, GeometryCombineMode.Exclude, null);
        }
    }
}
