﻿using ScreenShark.src.Helpers;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace ScreenShark.src.Models {
    class EditableImageCollection : Collection<EditableImage> {
        public BitmapSource CombineScreenshotsVertically() {
            int totalHeight = this.Sum(im => im.Image.PixelHeight);
            int maxWidth = this.Max(im => im.Image.PixelWidth);

            DrawingVisual visual = new();
            using (DrawingContext r = visual.RenderOpen()) {
                int offsetY = 0;
                foreach (EditableImage image in this) {
                    r.DrawImage(image.Render(), new Rect(0, offsetY, image.Image.PixelWidth, image.Image.PixelHeight));
                    offsetY += image.Image.PixelHeight;
                }
            }

            RenderTargetBitmap bitmap = RenderTargetBitmapHelpers.GetRenderTargetBitmap(maxWidth, totalHeight);
            bitmap.Render(visual);
            return bitmap;
        }

        public BitmapSource CombineScreenshotsHorizontally() {
            int totalWidth = this.Sum(im => im.Image.PixelWidth);
            int maxHeight = this.Max(im => im.Image.PixelHeight);

            DrawingVisual visual = new();
            using (DrawingContext r = visual.RenderOpen()) {
                int offsetX = 0;
                foreach (EditableImage image in this) {
                    r.DrawImage(image.Render(), new Rect(offsetX, 0, image.Image.PixelWidth, image.Image.PixelHeight));
                    offsetX += image.Image.PixelWidth;
                }
            }

            RenderTargetBitmap bitmap = RenderTargetBitmapHelpers.GetRenderTargetBitmap(totalWidth, maxHeight);
            bitmap.Render(visual);
            return bitmap;
        }
    }
}
