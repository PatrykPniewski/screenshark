﻿using CommunityToolkit.Mvvm.ComponentModel;
using ScreenShark.src.Helpers;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ScreenShark.src.Models {
    public partial class EditableImage : ObservableObject {
        [ObservableProperty]
        private BitmapSource image;
        [ObservableProperty]
        private ObservableCollection<Polyline> polylines = new();

        // Might need to set a max capacity
        // TODO: Create custom class for edits so you can undo clearing all polylines
        private readonly Stack<List<Polyline>> RemovedPolylines = new();

        public EditableImage(BitmapSource image) {
            this.image = image;
        }

        public void Undo() {
            if (Polylines.Count == 0) return;

            RemovedPolylines.Push(new List<Polyline>{ Polylines.Last() });
            Polylines.RemoveAt(Polylines.Count - 1);
        }

        public void Redo() {
            // Fires CollectionChanged event on every Add call, but shouldn't be a problem
            if (RemovedPolylines.TryPop(out List<Polyline> polylines))
                foreach (Polyline polyline in polylines)
                    Polylines.Add(polyline);
        }

        public void AddPolyline(Polyline line) {
            Polylines.Add(line);
            RemovedPolylines.Clear();
        }

        public void AddPointToLastPolyline(Point point) {
            Polylines.Last()?.Points.Add(point);
        }

        public void ClearPolylines() {
            RemovedPolylines.Push(Polylines.ToList());
            Polylines.Clear();
        }

        public BitmapSource Render() {
            DrawingVisual visual = new();

            using (DrawingContext r = visual.RenderOpen()) {
                r.DrawImage(Image, new Rect(0, 0, Image.PixelWidth, Image.PixelHeight));

                foreach (Polyline line in Polylines) {
                    PathGeometry geometry = new() {
                        Figures = new PathFigureCollection {
                            new PathFigure() {
                                StartPoint = new Point(line.Points[0].X, line.Points[0].Y),
                                Segments = new PathSegmentCollection { new PolyLineSegment(line.Points, true) }
                            }
                        }
                    };

                    Pen pen = new() {
                        Brush = line.Stroke,
                        Thickness = line.StrokeThickness,
                        StartLineCap = line.StrokeStartLineCap,
                        LineJoin = line.StrokeLineJoin,
                        EndLineCap = line.StrokeEndLineCap,
                    };

                    r.DrawGeometry(null, pen, geometry);
                }
            }

            RenderTargetBitmap renderTarget = RenderTargetBitmapHelpers.GetRenderTargetBitmap(Image);
            renderTarget.Render(visual);

            return renderTarget;
        }
    }
}
