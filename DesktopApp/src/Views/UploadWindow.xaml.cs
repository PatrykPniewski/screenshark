﻿using ScreenShark.src.ViewModels;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media.Imaging;

namespace ScreenShark.src.Views {
    /// <summary>
    /// Interaction logic for UploadWindow.xaml
    /// </summary>
    public partial class UploadWindow : Window {

        public UploadWindow(List<BitmapSource> frames) {
            this.DataContext = new UploadWindowViewModel(frames);
            InitializeComponent();
        }
    }
}
