﻿using ScreenShark.Properties;
using System.Windows;
using System.Windows.Input;

namespace ScreenShark.src.Views {
    /// <summary>
    /// Interaction logic for EditScreenshot.xaml
    /// </summary>
    public partial class EditScreenshot : Window {
        public EditScreenshot() {
            InitializeComponent();
        }

        /// <summary>
        /// Multiplies scale by <paramref name="scale"/>
        /// </summary>
        private void ChangeScale(float scale) {
            ImageScale.ScaleX *= scale;
            ImageScale.ScaleY *= scale;
        }

        // Additional Key Bindings
        private void CanvasMouseScroll(object sender, MouseWheelEventArgs e) {
            // Shift to scroll Horizontally
            if (Keyboard.Modifiers.HasFlag(ModifierKeys.Shift)) {
                PreviewScrollViewer.ScrollToHorizontalOffset(PreviewScrollViewer.HorizontalOffset - e.Delta);
                e.Handled = true;
                return;
            }

            // Ctrl + MouseWheel to zoom
            if (Keyboard.Modifiers.HasFlag(ModifierKeys.Control)) {
                if (e.Delta > 0) {
                    if (ImageScale.ScaleX > Settings.Default.MaxScale)
                        return;

                    ChangeScale(Settings.Default.ScaleStep);
                } else {
                    if (ImageScale.ScaleX < Settings.Default.MinScale)
                        return;

                    ChangeScale(1 / Settings.Default.ScaleStep);
                }
                return;
            }
        }
    }
}
