﻿using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace ScreenShark.src.Helpers {
    class RenderTargetBitmapHelpers {
        /// <returns><see cref="RenderTargetBitmap"/> compatible with given <paramref name="image"/></returns>
        public static RenderTargetBitmap GetRenderTargetBitmap(BitmapSource image) {
            return new RenderTargetBitmap(
                image.PixelWidth,
                image.PixelHeight,
                image.DpiX,
                image.DpiY,
                PixelFormats.Default
            );
        }

        /// <returns><see cref="RenderTargetBitmap"/> with given <paramref name="width"/>, <paramref name="height"/>, dpi of 96 and default pixelformat</returns>
        public static RenderTargetBitmap GetRenderTargetBitmap(int width, int height) {
            return new RenderTargetBitmap(
                width,
                height,
                96,
                96,
                PixelFormats.Default
            );
        }
    }
}
