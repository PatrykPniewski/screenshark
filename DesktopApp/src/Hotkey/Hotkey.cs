﻿using System;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Threading;
using Windows.Win32;
using Windows.Win32.Foundation;
using Windows.Win32.UI.Input.KeyboardAndMouse;

namespace ScreenShark.src.Hotkey {
    public class HotKey : IDisposable {
        private readonly int Id;
        private bool IsKeyRegistered;
        private readonly Dispatcher CurrentDispatcher;
        public event Action<HotKey> HotKeyPressed;
        private uint InteropKey => (uint) KeyInterop.VirtualKeyFromKey(Key);
        public Key Key { get; private set; }
        internal HOT_KEY_MODIFIERS ModifierKeys { get; private set; }

        internal HotKey(HOT_KEY_MODIFIERS modifierKeys, Key key) : this(modifierKeys, key, null) { }

        internal HotKey(HOT_KEY_MODIFIERS modifierKeys, Key key, Action<HotKey> onPress = null) {
            this.Key = key;
            this.ModifierKeys = modifierKeys;
            this.Id = GetHashCode();
            this.CurrentDispatcher = Dispatcher.CurrentDispatcher;
            RegisterHotKey();
            ComponentDispatcher.ThreadPreprocessMessage += ThreadPreprocessMessageMethod;

            if (onPress != null)
                HotKeyPressed += onPress;
        }

        ~HotKey() {
            Dispose();
        }

        public void Dispose() {
            try {
                ComponentDispatcher.ThreadPreprocessMessage -= ThreadPreprocessMessageMethod;
            } catch (Exception) {

            } finally {
                UnregisterHotKey();
            }
        }

        private void OnHotKeyPressed() {
            CurrentDispatcher.Invoke(() => {
                HotKeyPressed?.Invoke(this);
            });
        }

        private void RegisterHotKey() {
            if (Key == Key.None) {
                return;
            }

            if (this.IsKeyRegistered) {
                return;
            }

            this.IsKeyRegistered = PInvoke.RegisterHotKey(HWND.Null, this.Id, this.ModifierKeys, this.InteropKey);

            if (!this.IsKeyRegistered) {
                throw new ApplicationException("Unexpected Error");
            }
        }

        private void ThreadPreprocessMessageMethod(ref MSG msg, ref bool handled) {
            if (handled)
                return;

            if (msg.message != PInvoke.WM_HOTKEY || (int) msg.wParam != this.Id)
                return;

            OnHotKeyPressed();
            handled = true;
        }

        private void UnregisterHotKey() {
            IsKeyRegistered = PInvoke.UnregisterHotKey(HWND.Null, this.Id);
        }
    }
}
