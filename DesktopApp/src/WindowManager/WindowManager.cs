﻿using System;
using System.Linq;

namespace ScreenShark.src.WindowManagerNS {
    internal class WindowManager {
        public static T GetWindow<T>() {
            T window = App.Current.Windows.OfType<T>().FirstOrDefault();
            if (window != null)
                return window;

            return Activator.CreateInstance<T>();
        }
    }
}
