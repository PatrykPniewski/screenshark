﻿using System;
using System.IO;
using System.Windows.Media.Imaging;

namespace ScreenShark.src.Extensions {
    static class BitmapEncoderExtensions {
        /// <param name="ext"></param>
        /// <returns>Corresponding class extending <see cref="BitmapEncoder"/> </returns>
        /// <exception cref="NotImplementedException"></exception>
        public static BitmapEncoder GetEncoder(string ext) {
            if (ext[0] != '.')
                ext = $".{ext}";

            ext = ext.ToLowerInvariant();

            switch (ext) {
                case ".png":
                    return new PngBitmapEncoder();
                case ".gif":
                    return new GifBitmapEncoder();
                case ".tiff":
                    return new TiffBitmapEncoder();
                case ".jpeg":
                    return new JpegBitmapEncoder();
                case ".bmp":
                    return new BmpBitmapEncoder();
                case ".wmp":
                    return new WmpBitmapEncoder();
                default:
                    throw new NotImplementedException($"GetEncoder doesn't support {ext}");
            }
        }

        /// <summary>
        /// Saves <paramref name="encoder"/> to given <paramref name="filename"/>
        /// </summary>
        public static void SaveEncoder(this BitmapEncoder encoder, string filename) {
            using (FileStream fs = File.OpenWrite(filename)) {
                encoder.Save(fs);
            }
        }
    }
}
