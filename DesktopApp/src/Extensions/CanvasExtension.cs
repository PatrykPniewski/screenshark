﻿using ScreenShark.src.Helpers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace ScreenShark.src.Extensions {
    static class CanvasExtension {
        public static BitmapSource ToBitmapSource(this Canvas canvas, int width, int height) {
            VisualBrush brush = new VisualBrush(canvas);
            DrawingVisual visual = new DrawingVisual();
            using (DrawingContext dc = visual.RenderOpen()) {
                dc.DrawRectangle(brush, null, new Rect(new Point(0, 0), new Point(width, height)));
            }

            RenderTargetBitmap bmp = RenderTargetBitmapHelpers.GetRenderTargetBitmap(width, height);
            bmp.Render(visual);
            return bmp;
        }
    }
}
