﻿using ScreenShark.src.Interfaces;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace ScreenShark.src.Extensions {
    internal class EnumLocalizationExtension : MarkupExtension, ILocalizationExtension {
        public Type EnumType { get; set; }
        private object TargetObject;
        private object TargetProperty;
        private List<EnumDisplayMember> Members;

        public EnumLocalizationExtension() {
            Localization.LocalizationManager.Register(this);
        }

        public void Update() {
            if (TargetObject == null || TargetProperty == null)
                return;

            DependencyObject obj = (DependencyObject) TargetObject;
            DependencyProperty prop = (DependencyProperty) TargetProperty;

            if (Members == null)
                return;

            List<EnumDisplayMember> newMembers = GetValue();
            for (int i = 0; i < newMembers.Count; i++) {
                Members[i].Name = newMembers[i].Name;
            }

            if (obj is ComboBox) {
                ComboBox cbox = ((ComboBox) obj);
                string displayPath = cbox.DisplayMemberPath;
                // Refresh seleted item
                cbox.DisplayMemberPath = "";
                cbox.DisplayMemberPath = displayPath;

                // Refresh dropdown
                cbox.Items.Refresh();
            }

        }

        public List<EnumDisplayMember> GetValue() {
            List<EnumDisplayMember> res = new List<EnumDisplayMember>();
            foreach (string name in EnumType.GetEnumNames()) {
                string n = Localization.Resources.ResourceManager.GetString(name, Localization.Resources.Culture);
                object val = Enum.Parse(EnumType, name);
                res.Add(new EnumDisplayMember(n, val));
            }
            return res;
        }

        public override object ProvideValue(IServiceProvider serviceProvider) {
            IProvideValueTarget provideValueTarget = (IProvideValueTarget) serviceProvider.GetService(typeof(IProvideValueTarget));
            TargetObject = provideValueTarget.TargetObject;
            TargetProperty = provideValueTarget.TargetProperty;
            Members = GetValue();
            return Members;
        }

    }

    public class EnumDisplayMember {
        public string Name { get; set; }
        public object Value { get; set; }

        public EnumDisplayMember(string name, object value) {
            this.Name = name;
            this.Value = value;
        }
    }
}
