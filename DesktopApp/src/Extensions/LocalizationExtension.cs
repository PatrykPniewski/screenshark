﻿using ScreenShark.src.Interfaces;
using System;
using System.Windows;
using System.Windows.Markup;

namespace ScreenShark.src.Extensions {
    public class LocalizationExtension : MarkupExtension, ILocalizationExtension {
        public string Key { get; set; }
        private object TargetObject;
        private object TargetProperty;

        public LocalizationExtension() {
            Localization.LocalizationManager.Register(this);
        }

        public void Update() {
            if (TargetObject == null || TargetProperty == null)
                return;

            DependencyObject obj = (DependencyObject) TargetObject;
            DependencyProperty prop = (DependencyProperty) TargetProperty;

            obj.SetValue(prop, GetValue());
        }

        public object GetValue() {
            System.Globalization.CultureInfo culture = Localization.Resources.Culture;
            return Localization.Resources.ResourceManager.GetString(Key, culture);
        }

        public override object ProvideValue(IServiceProvider serviceProvider) {
            IProvideValueTarget provideValueTarget = (IProvideValueTarget) serviceProvider.GetService(typeof(IProvideValueTarget));
            TargetObject = provideValueTarget.TargetObject;
            TargetProperty = provideValueTarget.TargetProperty;
            return GetValue();
        }
    }
}
