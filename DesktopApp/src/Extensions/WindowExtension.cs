﻿using System.Windows;

namespace ScreenShark.src.Extensions {
    static internal class WindowExtension {
        public static void BringToFront(this Window window) {
            window.Visibility = Visibility.Visible;
            window.Activate();
        }

        public static void MakeHidden(this Window window) {
            window.Visibility = Visibility.Hidden;
        }
    }
}
