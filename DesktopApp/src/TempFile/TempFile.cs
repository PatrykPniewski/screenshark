﻿using ScreenShark.Properties;
using System;
using System.IO;

namespace ScreenShark.src.TempFileNS {
    public class TempFile : IDisposable {
        public string FilePath { get; private set; }
        public TempFile() { }

        ~TempFile() {
            Delete();
        }

        public void Dispose() {
            Delete();
            GC.SuppressFinalize(this);
        }

        public FileStream Create(string fileName) {
            if (FilePath != null)
                throw new Exception($"This TempFile instance already contains a file");

            FilePath = $"{Settings.Default.TempFolder}\\{fileName}";
            return File.Create(FilePath);
        }

        public void Delete() {
            if (!File.Exists(FilePath))
                return;

            File.Delete(FilePath);
            FilePath = null;
        }
    }
}
