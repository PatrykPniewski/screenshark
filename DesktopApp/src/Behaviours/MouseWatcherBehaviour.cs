﻿using Microsoft.Xaml.Behaviors;
using System.Windows;

namespace ScreenShark.src.Behaviours {
    public class MouseWatcherBehaviour : Behavior<UIElement> {
        public static readonly DependencyProperty MousePositionProperty = DependencyProperty.Register(nameof(MousePosition), typeof(Point), typeof(MouseWatcherBehaviour));
        public Point MousePosition {
            get => (Point) GetValue(MousePositionProperty);
            set => SetValue(MousePositionProperty, value);
        }

        protected override void OnAttached() {
            this.AssociatedObject.MouseMove += AssociatedObject_MouseMove;
        }

        protected override void OnDetaching() {
            this.AssociatedObject.MouseMove -= AssociatedObject_MouseMove;
        }

        private void AssociatedObject_MouseMove(object sender, System.Windows.Input.MouseEventArgs e) {
            MousePosition = e.GetPosition(this.AssociatedObject);
        }
    }
}
