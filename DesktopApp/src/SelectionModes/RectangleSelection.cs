﻿using System.Windows;
using System.Windows.Media;

namespace ScreenShark.src.SelectionModes {
    class RectangleSelection : SelectionMode {
        private Point startPoint;

        public override void Start(Point point) {
            if (IsRunning) return;

            base.Start(point);
            startPoint = point;
            Clip = new RectangleGeometry(new Rect(point, point));
        }

        public override void Update(Point point) {
            if (!IsRunning) return;

            Clip = new RectangleGeometry(new Rect(point, startPoint));
        }

        public override void Stop(Point point) {
            base.Stop(point);
        }
    }
}
