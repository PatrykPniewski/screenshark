﻿using CommunityToolkit.Mvvm.ComponentModel;
using System.Windows;
using System.Windows.Media;

namespace ScreenShark.src.SelectionModes {
    public abstract partial class SelectionMode : ObservableObject {
        public bool IsRunning { get; private set; } = false;

        [ObservableProperty]
        public Geometry clip;

        public virtual void Start(Point point) {
            IsRunning = true;
        }

        public virtual void Stop(Point point) {
            IsRunning = false;
        }

        public abstract void Update(Point point);
    }
}
