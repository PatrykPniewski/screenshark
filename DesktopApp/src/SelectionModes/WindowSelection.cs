﻿using ScreenShark.src.Views;
using ScreenShark.src.WindowManagerNS;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media;
using Windows.Win32;
using Windows.Win32.Foundation;
using Windows.Win32.Graphics.Dwm;
using Windows.Win32.UI.WindowsAndMessaging;

namespace ScreenShark.src.SelectionModes {
    class WindowSelection : SelectionMode {
        private List<HWND> windows = new();

        public override void Start(Point point) {
            if (IsRunning) return;

            windows = new();
            base.Start(point);
            SetClip(point);
        }

        public override void Update(Point point) {
            if (!IsRunning) Start(point);
            SetClip(point);
        }

        private HWND GetTopWindow(List<HWND> windows) {
            HWND topWindow = windows[0];
            HWND currWindow = topWindow;
            do {
                currWindow = PInvoke.GetWindow(currWindow, GET_WINDOW_CMD.GW_HWNDPREV);
                if (windows.Contains(currWindow))
                    topWindow = currWindow;
            } while (currWindow != HWND.Null);

            return topWindow;
        }

        private RECT? GetWindowRect(HWND hwnd) {
            RECT rect = new();
            unsafe {
                if (PInvoke.DwmGetWindowAttribute(hwnd, DWMWINDOWATTRIBUTE.DWMWA_EXTENDED_FRAME_BOUNDS, &rect, (uint) sizeof(RECT)).Value != HRESULT.S_OK)
                    return null;
            }
            return rect;
        }

        private List<HWND> GetWindows() {
            List<HWND> windows = new();
            PInvoke.EnumWindows((hwnd, b) => {
                if (!PInvoke.IsWindowVisible(hwnd))
                    return true;

                windows.Add(hwnd);
                return true;
            }, 0);

            return windows;
        }

        private HWND? GetWindow(Point point) {
            if (windows.Count == 0) {
                List<HWND> tempWindows = GetWindows();

                HWND topWindow = GetTopWindow(tempWindows);
                windows.Add(topWindow);
                HWND currWindow = topWindow;

                do {
                    currWindow = PInvoke.GetWindow(currWindow, GET_WINDOW_CMD.GW_HWNDNEXT);
                    if (tempWindows.Contains(currWindow))
                        windows.Add(currWindow);
                } while (currWindow != HWND.Null);
            }

            IntPtr handle = new WindowInteropHelper(WindowManager.GetWindow<SelectScreen>()).Handle;
            foreach (HWND hwnd in windows) {
                RECT? rect = GetWindowRect(hwnd);
                if (rect == null) continue;

                if (hwnd == handle) continue;
                if (point.X < rect.Value.X || point.X > rect.Value.X + rect.Value.Width) continue;
                if (point.Y < rect.Value.Y || point.Y > rect.Value.Y + rect.Value.Height) continue;

                return hwnd;
            }

            return null;
        }

        private void SetClip(Point point) {
            HWND? windowHandle = GetWindow(point);
            if (windowHandle == null)
                Clip = null;

            RECT? rect = GetWindowRect(windowHandle.Value) ?? throw new Exception("GetWindowRect returned false");

            Clip = new RectangleGeometry(new Rect(rect.Value.X, rect.Value.Y, rect.Value.Width, rect.Value.Height));
        }

        public override void Stop(Point point) {
            SetClip(point);
            base.Stop(point);
        }
    }
}
