﻿using System.Windows;
using System.Windows.Media;

namespace ScreenShark.src.SelectionModes {
    class PolylineSelection : SelectionMode {
        private PathGeometry _geometry;
        private PathFigure _pathFigure;
        private PolyLineSegment _polylineSegment;

        public override void Start(Point point) {
            if (IsRunning) return;

            base.Start(point);
            _geometry = new PathGeometry();
            _pathFigure = new PathFigure();
            _polylineSegment = new PolyLineSegment();

            _geometry.Figures.Add(_pathFigure);
            _pathFigure.Segments.Add(_polylineSegment);
            _pathFigure.StartPoint = point;
            _polylineSegment.Points = new PointCollection() { point };
            Clip = _geometry;
        }

        public override void Update(Point point) {
            if (!IsRunning) return;

            _polylineSegment.Points.Add(point);
            Clip = _geometry;
        }

        public override void Stop(Point point) {
            base.Stop(point);
        }
    }
}
