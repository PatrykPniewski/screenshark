﻿using System;

namespace ScreenShark.src.Enums {
    [Serializable]
    public enum FileSaveMode {
        CombineVertically,
        CombineHorizontally,
        MultipleFiles,
        OneFrame
    }
}
