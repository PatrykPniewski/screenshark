﻿using System;

namespace ScreenShark.src.Enums {
    [Serializable]
    public enum WindowShotMode : uint {
        ClientOnly = 1,
        FullContent = 2,
    }
}
