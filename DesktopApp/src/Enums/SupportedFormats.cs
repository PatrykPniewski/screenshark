﻿namespace ScreenShark.src.Enums {
    public enum SupportedFormats {
        PNG,
        JPEG,
        GIF,
        TIFF,
        BMP,
        WMP
    }
}
