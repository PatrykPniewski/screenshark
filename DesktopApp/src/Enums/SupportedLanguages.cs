﻿using System;

namespace ScreenShark.src.Enums {
    [Serializable]
    public enum SupportedLanguages {
        Default = 0,
        Polish = 0x0015,
        English = 0x0009,
    }
}
