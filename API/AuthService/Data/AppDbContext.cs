﻿using AuthService.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace AuthService.Data {
    public class AppDbContext : IdentityDbContext<User, Role, string> {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }
    }
}
