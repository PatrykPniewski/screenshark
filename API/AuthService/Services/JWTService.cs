﻿using AuthService.Models;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace AuthService.Services {
    public interface IJwtTokenService {
        string Generate(User user);
    }

    public class JWTService : IJwtTokenService {
        private readonly IConfiguration _configuration;

        public JWTService(IConfiguration configuration) {
            _configuration = configuration;
        }

        public string Generate(User user) {
            string jwtKey = _configuration["JWT_KEY"] ?? throw new KeyNotFoundException("JWT_KEY not found in config");
            SymmetricSecurityKey key = new(Encoding.UTF8.GetBytes(jwtKey));
            SigningCredentials signingCredentials = new(key, SecurityAlgorithms.HmacSha256);

            Claim[] claims = new[] {
                new Claim(ClaimTypes.Email, user.Email, ClaimValueTypes.Email),
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString(), ClaimValueTypes.String),
            };

            JwtSecurityToken token = new(
                issuer: _configuration["JWT_ISSUER"] ?? throw new KeyNotFoundException("JWT_ISSUER not found in config"),
                audience: _configuration["JWT_AUDIENCE"] ?? throw new KeyNotFoundException("JWT_AUDIENCE not found in config"),
                claims: claims,
                expires: DateTime.Now.AddDays(1),
                signingCredentials: signingCredentials
            );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}
