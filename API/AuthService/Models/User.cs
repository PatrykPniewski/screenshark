﻿using Microsoft.AspNetCore.Identity;

namespace AuthService.Models {
    public class User : IdentityUser {
        public User(string userName, string email) {
            Id = Guid.NewGuid().ToString();
            UserName = userName;
            Email = email;
        }
    }
}
