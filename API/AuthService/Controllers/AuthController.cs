﻿using AuthService.Models;
using AuthService.Services;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Shared.DTOs.Auth;
using Shared.Enums;
using System.Net.Mime;

namespace AuthService.Controllers {
    /// <summary>
    /// Authentication controller
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    [Consumes(MediaTypeNames.Application.Json)]
    [Produces(MediaTypeNames.Application.Json)]
    public class AuthController : ControllerBase {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly IJwtTokenService _jwtTokenService;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userManager"></param>
        /// <param name="signInManager"></param>
        /// <param name="jwtTokenService"></param>
        public AuthController(UserManager<User> userManager, SignInManager<User> signInManager, IJwtTokenService jwtTokenService) {
            _userManager = userManager;
            _signInManager = signInManager;
            _jwtTokenService = jwtTokenService;
        }

        /// <summary>
        /// Registers a user
        /// </summary>
        /// <param name="userDTO"></param>
        /// <returns></returns>
        /// <response code="204"></response>
        /// <response code="400">If user provided invalid data</response>
        [HttpPost]
        [ProducesResponseType(typeof(void), StatusCodes.Status204NoContent)]
        [ProducesResponseType(typeof(RegisterErrorResponse), StatusCodes.Status400BadRequest)]
        async public Task<Results<NoContent, BadRequest<RegisterErrorResponse>>> Register(UserRegisterDTO userDTO) {
            User user = new(userDTO.UserName, userDTO.Email);

            IdentityResult result = await _userManager.CreateAsync(user, userDTO.Password);
            if (result.Succeeded)
                return TypedResults.NoContent();
            
            return TypedResults.BadRequest(new RegisterErrorResponse(GetErrors(result.Errors)));
        }

        /// <summary>
        /// Returns a token if credentials are valid
        /// </summary>
        /// <param name="userDTO"></param>
        /// <returns></returns>
        /// <response code="200"></response>
        /// <response code="404">If the email password combination is not found</response>
        [HttpPost]
        [ProducesResponseType(typeof(LoginResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        public async Task<Results<Ok<LoginResponse>, NotFound>> Login(UserLoginDTO userDTO) {
            User? user = await _userManager.FindByEmailAsync(userDTO.Email);
            if (user == null)
                return TypedResults.NotFound();
            
            var result = await _signInManager.CheckPasswordSignInAsync(user, userDTO.Password, false);
            if (result.Succeeded)
                return TypedResults.Ok(new LoginResponse(_jwtTokenService.Generate(user)));

            return TypedResults.NotFound();
        }

        private IEnumerable<ErrorCode> GetErrors(IEnumerable<IdentityError> errors) {
            return errors.Select(err => {
                if (Enum.TryParse(err.Code, out ErrorCode ec))
                    return ec;
                return ErrorCode.UnknownError;
            });
        }
    }
}
