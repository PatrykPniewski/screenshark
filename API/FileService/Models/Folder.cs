﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;

namespace FileService.Models {
    public class Folder {
        [MaxLength(36), MinLength(36)]
        public string Id { get; set; }
        [MaxLength(36), MinLength(36)]
        public string UserId { get; set; }
        [MinLength(1), MaxLength(64)]
        public string Name { get; set; }
        public string? FolderId { get; set; }

        [DeleteBehavior(DeleteBehavior.NoAction)]
        public virtual List<File> Files { get; set; } = new List<File>();
        [DeleteBehavior(DeleteBehavior.NoAction)]
        public virtual List<Folder> Folders { get; set; } = new List<Folder>();

        public Folder(string name, string userId, string? folderId) {
            Id = Guid.NewGuid().ToString();
            Name = name;
            FolderId = folderId;
            UserId = userId;
        }
    }
}
