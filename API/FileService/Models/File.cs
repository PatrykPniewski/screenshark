﻿using Microsoft.EntityFrameworkCore;
using Shared.Enums;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace FileService.Models {
    [Index(nameof(CreatedAt))]
    public class File {
        [MaxLength(36), MinLength(36)]
        public string Id { get; set; }
        [MaxLength(36), MinLength(36)]
        public string UserId { get; set; }  
        public string? FolderId { get; set; }
        [MinLength(1), MaxLength(64)]
        public string Name { get; set; }
        [MinLength(1), MaxLength(6)]
        public string Extension { get; set; }
        [DefaultValue(FileProtection.Private)]
        public FileProtection Protection { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? LastUpdated { get; set; }
        public uint DownloadCount { get; set; } = 0;

        public virtual Folder? Folder { get; set; }

        public File(string name, string userId, FileProtection protection, string extension, string? folderId = null) {
            Id = Guid.NewGuid().ToString();
            Name = name;
            UserId = userId;
            Protection = protection;
            Extension = extension;
            CreatedAt = DateTime.Now;
            FolderId = folderId;
        }

        public File(string id, string name, string userId, FileProtection protection, string extension, string? folderId = null) : this(name, userId, protection, extension, folderId) {
            Id = id;
        }
    }
}
