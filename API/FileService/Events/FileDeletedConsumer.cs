﻿using FileService.Repositories;
using MassTransit;
using Shared.Events;

namespace FileService.Events {
    public class FileDeletedConsumer : IConsumer<FileDeletedEvent> {
        private readonly IFileRepository _fileRepository;

        public FileDeletedConsumer(IFileRepository fileRepository) {
            _fileRepository = fileRepository;
        }

        public async Task Consume(ConsumeContext<FileDeletedEvent> context) {
            await _fileRepository.Delete(context.Message.FileId);
        }
    }
}
