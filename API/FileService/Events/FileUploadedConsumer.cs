﻿using FileService.Repositories;
using MassTransit;
using Shared.Events;

namespace FileService.Events {
    public class FileUploadedConsumer : IConsumer<FileUploadedEvent> {
        private readonly IFileRepository _fileRepository;

        public FileUploadedConsumer(IFileRepository fileRepository) {
            _fileRepository = fileRepository;
        }

        public async Task Consume(ConsumeContext<FileUploadedEvent> context) {
            var file = context.Message;
            await _fileRepository.Add(
                new Models.File(
                    file.FileId,
                    file.FileName,
                    file.UserId,
                    file.FileProtection,
                    Path.GetExtension(file.FileName),
                    file.FolderId)
                );
        }
    }
}
