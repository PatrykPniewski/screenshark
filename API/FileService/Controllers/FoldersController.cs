﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using FileService.Data;
using Shared.Extensions;
using FileService.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Shared.DTOs.File;
using Shared.DTOs.Folder;
using System.Net.Mime;

namespace FileService.Controllers {
    /// <summary>
    /// Controller for folders
    /// </summary>
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    [Consumes(MediaTypeNames.Application.Json)]
    [Produces(MediaTypeNames.Application.Json)]
    public class FoldersController : ControllerBase {
        private readonly AppDbContext _context;
        private readonly IMapper _mapper;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="mapper"></param>
        public FoldersController(AppDbContext context, IMapper mapper) {
            _context = context ?? throw new ArgumentNullException(nameof(context)); ;
            _mapper = mapper;
        }

        // GET: api/Folders/5
        /// <summary>
        /// Gets a list of folders and files in specified directory
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <response code="200"></response>
        /// <response code="401">If user is not logged in or doesn't have NameIdentifier claim</response>
        [HttpGet("{id?}")]
        [ProducesResponseType(typeof(void), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status401Unauthorized)]
        public async Task<Results<UnauthorizedHttpResult, Ok<GetFolderContentsDTO>>> GetFolderContents(string? id = null) {
            string? userId = this.GetUserId();
            if (userId == null)
                return TypedResults.Unauthorized();

            List<GetFolderDTO> folders = await _context.Folders
                .Where(folder => folder.UserId == userId && folder.FolderId == id)
                .ProjectTo<GetFolderDTO>(_mapper.ConfigurationProvider)
                .ToListAsync();

            List<GetFileDTO> files = await _context.Files
                .Where(file => file.UserId == userId && file.FolderId == id)
                .ProjectTo<GetFileDTO>(_mapper.ConfigurationProvider)
                .ToListAsync();

            GetFolderContentsDTO dto = new(files, folders);
            return TypedResults.Ok(dto);
        }

        // PUT: api/Folders/5
        /// <summary>
        /// Updates information about file
        /// </summary>
        /// <param name="id"></param>
        /// <param name="folderDTO"></param>
        /// <returns></returns>
        /// <response code="204"></response>
        /// <response code="401"></response>
        /// <response code="403">User doesn't own folder</response>
        /// <response code="404"></response>
        [HttpPut("{id}")]
        [ProducesResponseType(typeof(void), StatusCodes.Status204NoContent)]
        [ProducesResponseType(typeof(void), StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(typeof(void), StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        public async Task<Results<NotFound, ForbidHttpResult, NoContent>> PutFolder(string id, UpdateFolderDTO folderDTO) {
            var folder = await _context.Folders.FindAsync(id);

            if (folder == null)
                return TypedResults.NotFound();

            string? UserId = this.GetUserId();
            if (folder.UserId != UserId)
                return TypedResults.Forbid();

            folder.Name = folderDTO.Name;
            _context.Entry(folder).State = EntityState.Modified;

            try {
                await _context.SaveChangesAsync();
            } catch (DbUpdateConcurrencyException) {
                if (!FolderExists(id)) {
                    return TypedResults.NotFound();
                } else {
                    throw;
                }
            }

            return TypedResults.NoContent();
        }

        // POST: api/Folders
        /// <summary>
        /// Creates a new folder
        /// </summary>
        /// <param name="folderDTO"></param>
        /// <returns></returns>
        /// <response code="204"></response>
        /// <response code="401">User is not logged in or doesn't have NameIdentifier claim</response>
        [HttpPost]
        [ProducesResponseType(typeof(void), StatusCodes.Status204NoContent)]
        [ProducesResponseType(typeof(void), StatusCodes.Status401Unauthorized)]
        public async Task<Results<UnauthorizedHttpResult, NoContent>> PostFolder(InsertFolderDTO folderDTO) {
            string? userId = this.GetUserId();
            if (userId == null)
                return TypedResults.Unauthorized();

            _context.Folders.Add(new Folder(folderDTO.Name, userId, folderDTO.FolderId));
            await _context.SaveChangesAsync();

            return TypedResults.NoContent();
        }

        // DELETE: api/Folders/5
        /// <summary>
        /// Deletes a folder
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <response code="204"></response>
        /// <response code="401"></response>
        /// <response code="403">User does not own folder</response>
        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(void), StatusCodes.Status204NoContent)]
        [ProducesResponseType(typeof(void), StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(typeof(void), StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        public async Task<Results<NotFound, ForbidHttpResult, NoContent>> DeleteFolder(string id) {
            // TODO delete recursively files and folders from azure storage
            var folder = await _context.Folders.FindAsync(id);
            if (folder == null) {
                return TypedResults.NotFound();
            }

            if (this.GetUserId() != folder.UserId)
                return TypedResults.Forbid();

            _context.Folders.Remove(folder);
            await _context.SaveChangesAsync();

            return TypedResults.NoContent();
        }

        private bool FolderExists(string id) {
            return (_context.Folders?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
