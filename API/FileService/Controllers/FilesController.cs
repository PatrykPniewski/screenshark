﻿using AutoMapper;
using FileService.Repositories;
using MassTransit;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Shared.DTOs.File;
using Shared.DTOs.Report;
using Shared.Enums;
using Shared.Events;
using Shared.Extensions;
using Shared.Messages.GenerateLink;
using System.Net.Mime;

namespace FileService.Controllers {
    /// <summary>
    /// Controller handling files
    /// </summary>
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    public class FilesController : ControllerBase {
        private readonly IMapper _mapper;
        private readonly Options.FileOptions _options;
        private readonly IPublishEndpoint _publishEndpoint;
        private readonly IRequestClient<GenerateDownloadLinkEvent> _requestClient;
        private readonly IFileRepository _fileRepository;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mapper"></param>
        /// <param name="options"></param>
        /// <param name="publishEndpoint"></param>
        /// <param name="client"></param>
        /// <param name="fileRepository"></param>
        /// <exception cref="ArgumentNullException"></exception>
        public FilesController(IMapper mapper, IOptions<Options.FileOptions> options, IPublishEndpoint publishEndpoint, IRequestClient<GenerateDownloadLinkEvent> client, IFileRepository fileRepository) {
            _mapper = mapper;
            _options = options.Value;
            _publishEndpoint = publishEndpoint;
            _requestClient = client;
            _fileRepository = fileRepository;
        }

        // GET: api/Files
        /// <summary>
        /// Gets a list of user files
        /// </summary>
        /// <returns>List of user files</returns>
        /// <response code="200">Returns a list of user files</response>
        /// <response code="401">Not logged in or If there's no NameIdentifier claim</response>
        [HttpGet]
        [ProducesResponseType(typeof(List<GetFileDTO>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status401Unauthorized)]
        [Produces(MediaTypeNames.Application.Json)]
        public async Task<Results<ProblemHttpResult, UnauthorizedHttpResult, Ok<List<GetFileDTO>>>> GetOwnedFiles() {
            string? UserId = this.GetUserId();
            if (UserId == null)
                return TypedResults.Unauthorized();

            return TypedResults.Ok(await _fileRepository.GetUserFiles(UserId));
        }

        /// <summary>
        /// Gets download link
        /// </summary>
        /// <param name="id">File id</param>
        /// <param name="cancellationToken"></param>
        /// <response code="200">Returns download link</response>
        /// <response code="401"></response>
        /// <response code="403"></response>
        /// <response code="404">File with specified id doesn't exist</response>
        /// <response code="500">Failed to generate link</response>
        [AllowAnonymous]
        [HttpGet("{id}/GenerateDownloadLink")]
        [ProducesResponseType(typeof(GetDownloadLinkDTO), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(typeof(void), StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status500InternalServerError)]
        public async Task<Results<NotFound, Ok<GetDownloadLinkDTO>, ProblemHttpResult, UnauthorizedHttpResult, ForbidHttpResult>> DownloadFile(string id, CancellationToken cancellationToken) {
            string? UserId = this.GetUserId();
            GetFileDTO? file = await _fileRepository.Find(id);

            if (file == null)
                return TypedResults.NotFound();

            if (UserId == null && file.Protection > FileProtection.Public)
                return TypedResults.Unauthorized();

            if (file.Protection == FileProtection.Private && UserId != file.UserId)
                return TypedResults.Forbid();

            var response = await _requestClient.GetResponse<GenerateDownloadLinkResult>(new(file.Id, UserId, file.Name, file.Protection, file.UserId), cancellationToken);

            if (!response.Message.Success) {
                return TypedResults.Problem("Couldn't generate download link");
            }

            return TypedResults.Ok(new GetDownloadLinkDTO(response.Message.DownloadId, response.Message.ExpirationDate));
        }

        // GET: api/Files/Page/5
        /// <summary>
        /// Gets a list of files on specified page, a not logged in user can only view first page
        /// </summary>
        /// <param name="page"></param>
        /// <returns>A list of files</returns>
        /// <response code="200">returns a list of files on specified page</response>
        [AllowAnonymous]
        [HttpGet("Page/{page:int:min(0)?}")]
        [ProducesResponseType(typeof(List<GetFileDTO>), StatusCodes.Status200OK)]
        [Produces(MediaTypeNames.Application.Json)]
        public async Task<Results<UnauthorizedHttpResult, Ok<List<GetFileDTO>>>> GetFilesByPage(int? page) {
            string? userId = this.GetUserId();
            if (userId == null)
                return TypedResults.Ok(await _fileRepository.GetFilesOnPage(0, _options.FilesPerPage));

            return TypedResults.Ok(await _fileRepository.GetFilesOnPage(page ?? 0, _options.FilesPerPage, FileProtection.RequireLogin));
        }

        // GET: api/Files/Page/Next
        /// <summary>
        /// Gets a list of files that are older than specified date time
        /// </summary>
        /// <param name="lastViewedFileInsertTime"></param>
        /// <returns>List of files</returns>
        /// <response code="200">Returns list of files that happened before specified DateTime</response>
        /// <response code="401">User is not logged in</response>
        [HttpGet("Page/Next")]
        [ProducesResponseType(typeof(List<GetFileDTO>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status401Unauthorized)]
        [Produces(MediaTypeNames.Application.Json)]
        public async Task<Results<UnauthorizedHttpResult, Ok<List<GetFileDTO>>>> NextPage(DateTime lastViewedFileInsertTime) {
            return TypedResults.Ok(await _fileRepository.GetOlderFiles(lastViewedFileInsertTime, _options.FilesPerPage, FileProtection.RequireLogin));
        }

        // GET: api/Files/Page/Previous
        /// <summary>
        /// Gets a list of files that are newer than specified date time
        /// </summary>
        /// <param name="firstViewedFileInsertTime"></param>
        /// <returns>List of files</returns>
        /// <response code="200">Returns list of files that happened after specified DateTime</response>
        /// <response code="401">User is not logged in</response>
        [HttpGet("Page/Previous")]
        [ProducesResponseType(typeof(List<GetFileDTO>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status401Unauthorized)]
        [Produces(MediaTypeNames.Application.Json)]
        public async Task<Results<UnauthorizedHttpResult, Ok<List<GetFileDTO>>>> PreviousPage(DateTime firstViewedFileInsertTime) {
            return TypedResults.Ok(await _fileRepository.GetNewerFiles(firstViewedFileInsertTime, _options.FilesPerPage, FileProtection.RequireLogin));
        }

        // GET: api/Files/5
        /// <summary>
        /// Gets info about specified file
        /// </summary>
        /// <param name="id"></param>
        /// <returns>File info</returns>
        /// <response code="200">Info about file</response>
        /// <response code="403">If file is private and not owned by user</response>
        /// <response code="404">If not found</response>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(List<GetFileDTO>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(typeof(void), StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        [Produces(MediaTypeNames.Application.Json)]
        public async Task<Results<NotFound, ForbidHttpResult, Ok<GetFileDTO>>> GetFile(string id) {
            var file = await _fileRepository.Find(id);

            if (file == null)
                return TypedResults.NotFound();

            string? UserId = this.GetUserId();
            if (file.Protection == FileProtection.Private && UserId != file.UserId)
                return TypedResults.Forbid();

            return TypedResults.Ok(_mapper.Map<GetFileDTO>(file));
        }

        // POST: api/Files/5/Report
        /// <summary>
        /// Reports a file
        /// </summary>
        /// <param name="fileId"></param>
        /// <param name="report"></param>
        /// <returns></returns>
        /// <response code="204"></response>
        /// <response code="401">Not logged in or user claim doesn't have NameIdentifier</response>
        /// <response code="404"></response>
        [HttpPost("{fileId}/Report")]
        [ProducesResponseType(typeof(void), StatusCodes.Status204NoContent)]
        [ProducesResponseType(typeof(void), StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        [Consumes(MediaTypeNames.Application.Json)]
        public async Task<Results<UnauthorizedHttpResult, NotFound, NoContent>> PostReport(string fileId, InsertReportDTO report) {
            string? userId = this.GetUserId();
            if (userId == null)
                return TypedResults.Unauthorized();

            var file = await _fileRepository.Find(fileId);
            if (file == null)
                return TypedResults.NotFound();

            await _publishEndpoint.Publish(new FileReportedEvent(fileId, userId, report.Comment));
            return TypedResults.NoContent();
        }

        // PUT: api/Files/5
        /// <summary>
        /// Updates information about a file
        /// </summary>
        /// <param name="id"></param>
        /// <param name="fileDTO"></param>
        /// <returns></returns>
        /// <response code="204"></response>
        /// <response code="401">User not logged in</response>
        /// <response code="403">User doesn't own this file</response>
        /// <response code="404"></response>
        [HttpPut("{id}")]
        [ProducesResponseType(typeof(void), StatusCodes.Status204NoContent)]
        [ProducesResponseType(typeof(void), StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(typeof(void), StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        [Consumes(MediaTypeNames.Application.Json)]
        public async Task<Results<NoContent, ForbidHttpResult, UnauthorizedHttpResult, NotFound>> PutFile(string id, UpdateFileDTO fileDTO) {
            var file = await _fileRepository.Find(id);

            if (file == null)
                return TypedResults.NotFound();

            string? UserId = this.GetUserId();
            if (file.UserId != UserId)
                return TypedResults.Forbid();

            await _fileRepository.Update(id, fileDTO);
            return TypedResults.NoContent();
        }

        // DELETE: api/Files/5
        /// <summary>
        /// Deletes a file
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <response code="204"></response>
        /// <response code="401">User not logged in</response>
        /// <response code="403">User doesn't own file</response>
        /// <response code="404"></response>
        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(void), StatusCodes.Status204NoContent)]
        [ProducesResponseType(typeof(void), StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(typeof(void), StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        public async Task<Results<NoContent, ForbidHttpResult, NotFound, UnauthorizedHttpResult>> DeleteFile(string id) {
            var file = await _fileRepository.Find(id);
            if (file == null) {
                return TypedResults.NotFound();
            }

            string? UserId = this.GetUserId();
            if (UserId != file.UserId)
                return TypedResults.Forbid();

            await _publishEndpoint.Publish(new FileDeletionRequestedEvent(UserId, id));
            return TypedResults.NoContent();
        }
    }
}
