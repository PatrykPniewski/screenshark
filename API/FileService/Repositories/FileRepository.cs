﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using FileService.Data;
using Microsoft.EntityFrameworkCore;
using Shared.DTOs.File;
using Shared.Enums;

namespace FileService.Repositories {
    public class FileRepository : IFileRepository {
        private readonly IMapper _mapper;
        private readonly AppDbContext _context;

        public FileRepository(IMapper mapper, AppDbContext context) {
            _mapper = mapper;
            _context = context;
        }

        public async Task<GetFileDTO?> Find(string fileId) {
            Models.File? file = await _context.Files.FindAsync(fileId);
            if (file == null)
                return null;

            return _mapper.Map<GetFileDTO>(file);
        }

        public async Task<bool> Update(string fileId, UpdateFileDTO fileDTO) {
            int updatedFiles = await _context.Files
                .ExecuteUpdateAsync(s => s
                    .SetProperty(e => e.Name, fileDTO.Name)
                    .SetProperty(e => e.Protection, fileDTO.Protection)
                    .SetProperty(e => e.LastUpdated, DateTime.Now)
                 );

            return true;
        }

        public async Task<bool> Add(Models.File file) {
            _context.Files.Add(file);
            await _context.SaveChangesAsync();

            return true;
        }

        public async Task<bool> Delete(string fileId) {
            await _context.Files.Where(f => f.Id == fileId).ExecuteDeleteAsync();

            return true;
        }

        public async Task<List<GetFileDTO>> GetUserFiles(string userId) {
            return await _context.Files
                .Where(f => f.UserId == userId)
                .ProjectTo<GetFileDTO>(_mapper.ConfigurationProvider)
                .ToListAsync();
        }

        public async Task<List<GetFileDTO>> GetFilesOnPage(int page, int itemsPerPage, FileProtection fileProtection) {
            return await _context.Files
                .Where(f => f.Protection <= fileProtection)
                .OrderByDescending(f => f.CreatedAt)
                .Skip(itemsPerPage * page)
                .Take(itemsPerPage)
                .ProjectTo<GetFileDTO>(_mapper.ConfigurationProvider)
                .ToListAsync();
        }

        public async Task<List<GetFileDTO>> GetOlderFiles(DateTime time, int itemsPerPage, FileProtection fileProtection) {
            return await _context.Files
                .Where(f => f.Protection <= fileProtection && f.CreatedAt < time)
                .OrderByDescending(f => f.CreatedAt)
                .Take(itemsPerPage)
                .ProjectTo<GetFileDTO>(_mapper.ConfigurationProvider)
                .ToListAsync();
        }

        public async Task<List<GetFileDTO>> GetNewerFiles(DateTime time, int itemsPerPage, FileProtection fileProtection) {
            return await _context.Files
                .Where(f => f.Protection <= fileProtection && f.CreatedAt > time)
                .OrderBy(f => f.CreatedAt)
                .Take(itemsPerPage)
                .ProjectTo<GetFileDTO>(_mapper.ConfigurationProvider)
                .ToListAsync();
        }
    }
}
