﻿using Shared.DTOs.File;
using Shared.Enums;

namespace FileService.Repositories {
    public interface IFileRepository {
        Task<GetFileDTO?> Find(string fileId);
        Task<List<GetFileDTO>> GetFilesOnPage(int page, int itemsPerPage, FileProtection fileProtection = FileProtection.Public);
        Task<List<GetFileDTO>> GetNewerFiles(DateTime time, int itemsPerPage, FileProtection fileProtection = FileProtection.Public);
        Task<List<GetFileDTO>> GetOlderFiles(DateTime time, int itemsPerPage, FileProtection fileProtection = FileProtection.Public);
        Task<List<GetFileDTO>> GetUserFiles(string userId);
        Task<bool> Update(string fileId, UpdateFileDTO fileDTO);
        Task<bool> Delete(string fileId);
        Task<bool> Add(Models.File file);
    }
}