using AutoMapper;
using FileService.Data;
using MassTransit;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Azure;
using Microsoft.OpenApi.Models;
using Shared.DTOs.File;
using Shared.DTOs.Folder;
using Shared.Extensions;
using System.Reflection;
using Shared.Services.FileStorageService;
using FileService.Events;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddDbContext<AppDbContext>(options => {
    options.UseSqlServer(builder.Configuration["DB_CONN_STR"] ?? throw new KeyNotFoundException("DB_CONN_STR not found in config/env"));
});

builder.Services.AddAzureClients(clientBuilder => {
    clientBuilder.AddBlobServiceClient(builder.Configuration["AZURITE_BLOB_CONN_STR"] ?? throw new KeyNotFoundException("AZURITE_BLOB_CONN_STR not found in config/env"), preferMsi: true);
    clientBuilder.AddQueueServiceClient(builder.Configuration["AZURITE_QUEUE_CONN_STR"] ?? throw new KeyNotFoundException("AZURITE_QUEUE_CONN_STR not found in config/env"), preferMsi: true);
});

builder.Services.AddJwtAuth();

builder.Services.Configure<FileService.Options.FileOptions>(
    builder.Configuration.GetSection(FileService.Options.FileOptions.SectionName)
);

var mapperConfig = new MapperConfiguration(cfg => {
    cfg.CreateMap<FileService.Models.File, GetFileDTO>();
    cfg.CreateMap<FileService.Models.Folder, GetFolderDTO>();
    cfg.CreateMap<FileService.Models.Folder, GetFolderContentsDTO>();
});
var mapper = mapperConfig.CreateMapper();

builder.Services.AddMassTransit(bus => {
    bus.UsingRabbitMq((context, cfg) => {
        cfg.Host(new Uri(builder.Configuration["RABBITMQ_CONN_STR"] ?? throw new KeyNotFoundException("RABBITMQ_CONN_STR not found in config/env")));
        cfg.ConfigureEndpoints(context);
    });

    bus.AddConsumer<FileUploadedConsumer>();
    bus.AddConsumer<FileDeletedConsumer>();
});

builder.Services.AddSingleton(mapper);

builder.Services.AddControllers();

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();

builder.Services.AddSwaggerGen(setup => {
    // Include 'SecurityScheme' to use JWT Authentication
    var jwtSecurityScheme = new OpenApiSecurityScheme {
        BearerFormat = "JWT",
        Name = "JWT Authentication",
        In = ParameterLocation.Header,
        Type = SecuritySchemeType.Http,
        Scheme = JwtBearerDefaults.AuthenticationScheme,
        Description = "JWT Token here",

        Reference = new OpenApiReference {
            Id = JwtBearerDefaults.AuthenticationScheme,
            Type = ReferenceType.SecurityScheme
        }
    };

    setup.AddSecurityDefinition(jwtSecurityScheme.Reference.Id, jwtSecurityScheme);

    setup.AddSecurityRequirement(new OpenApiSecurityRequirement {
        { jwtSecurityScheme, Array.Empty<string>() }
    });

    var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
    var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
    setup.IncludeXmlComments(xmlPath);
});

var app = builder.Build();

using (var serviceScope = app.Services.CreateScope()) {
    var dbContext = serviceScope.ServiceProvider.GetRequiredService<AppDbContext>();
    await dbContext.Database.EnsureCreatedAsync();
}

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment()) {
    app.UseSwagger();
    app.UseSwaggerUI();
}
app.UseHttpsRedirection();
app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

app.Run();
