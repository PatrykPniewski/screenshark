﻿namespace FileService.Options {
    public class FileOptions {
        public const string SectionName = "File";

        public int FilesPerPage { get; set; } = 10;
    }
}
