﻿using Microsoft.EntityFrameworkCore;

namespace FileService.Data {
    public class AppDbContext : DbContext {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }

        public DbSet<Models.File> Files => Set<Models.File>();
        public DbSet<Models.Folder> Folders => Set<Models.Folder>();
    }
}
