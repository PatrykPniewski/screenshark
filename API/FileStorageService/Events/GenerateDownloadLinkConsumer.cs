﻿using FileStorageService.Models;
using FileStorageService.Repositories;
using MassTransit;
using Shared.Messages.GenerateLink;

namespace FileStorageService.Events {
    public class GenerateDownloadLinkConsumer : IConsumer<GenerateDownloadLinkEvent> {
        private ILinkRepository _linkRepository;

        public GenerateDownloadLinkConsumer(ILinkRepository linkRepository) {
            _linkRepository = linkRepository;
        }

        public async Task Consume(ConsumeContext<GenerateDownloadLinkEvent> context) {
            GenerateDownloadLinkEvent data = context.Message;

            string id = Guid.NewGuid().ToString();
            DateTime expirationDate = DateTime.Now.Add(TimeSpan.FromMinutes(15));

            bool wasCreated = await _linkRepository.CreateTemporaryLink(new TemporaryDownloadLink(id, data.FileId, data.Name, data.FileProtection, expirationDate, data.OwnerUserId, data.RequestedByUserId));

            await context.RespondAsync(new GenerateDownloadLinkResult(id, wasCreated, expirationDate));
        }
    }
}
