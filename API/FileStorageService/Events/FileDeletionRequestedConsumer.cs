﻿using MassTransit;
using Shared.Events;
using Shared.Services.FileStorageService;

namespace FileStorageService.Events {
    public class FileDeletionRequestedConsumer(IFileStorageService fileStorageService) : IConsumer<FileDeletionRequestedEvent> {
        private IFileStorageService _fileStorageService = fileStorageService;

        public async Task Consume(ConsumeContext<FileDeletionRequestedEvent> context) {
            var file = context.Message;

            var deleted = await _fileStorageService.Delete(file.UserId, file.FileId);
            if (!deleted) {
                await context.Publish(new FileDeletionErrorEvent(file.FileId, file.UserId));
                return;
            }

            await context.Publish(new FileDeletedEvent(file.FileId));
        }
    }
}
