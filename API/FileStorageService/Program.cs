using FileStorageService.Events;
using FileStorageService.Repositories;
using MassTransit;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.OpenApi.Models;
using Shared.Extensions;
using Shared.Messages.GenerateLink;
using StackExchange.Redis;
using System.Reflection;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddSingleton<IConnectionMultiplexer>(cfg => {
    return ConnectionMultiplexer.Connect(builder.Configuration["DB_CONN_STR"] ?? throw new KeyNotFoundException("DB_CONN_STR not found in config/env"));
});

builder.Services.AddMassTransit(bus => {
    bus.UsingRabbitMq((context, cfg) => {
        cfg.Host(new Uri(builder.Configuration["RABBITMQ_CONN_STR"] ?? throw new KeyNotFoundException("RABBITMQ_CONN_STR not found in config/env")));
        cfg.ConfigureEndpoints(context);
    });

    bus.AddConsumer<GenerateDownloadLinkConsumer>()
        .Endpoint(e => e.Name = "generate-download-link");
    bus.AddConsumer<FileDeletionRequestedConsumer>();
});

builder.Services.AddSingleton<ILinkRepository, LinkRepository>();

builder.Services.AddJwtAuth();

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(setup => {
    // Include 'SecurityScheme' to use JWT Authentication
    var jwtSecurityScheme = new OpenApiSecurityScheme {
        BearerFormat = "JWT",
        Name = "JWT Authentication",
        In = ParameterLocation.Header,
        Type = SecuritySchemeType.Http,
        Scheme = JwtBearerDefaults.AuthenticationScheme,
        Description = "JWT Token here",

        Reference = new OpenApiReference {
            Id = JwtBearerDefaults.AuthenticationScheme,
            Type = ReferenceType.SecurityScheme
        }
    };

    setup.AddSecurityDefinition(jwtSecurityScheme.Reference.Id, jwtSecurityScheme);

    setup.AddSecurityRequirement(new OpenApiSecurityRequirement {
        { jwtSecurityScheme, Array.Empty<string>() }
    });

    var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
    var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
    setup.IncludeXmlComments(xmlPath);
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment()) {
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
