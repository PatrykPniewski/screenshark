﻿using FileStorageService.Models;
using StackExchange.Redis;
using System.Text.Json;

namespace FileStorageService.Repositories {
    public interface ILinkRepository {
        Task<bool> CreateTemporaryLink(TemporaryDownloadLink link);
        Task<TemporaryDownloadLink?> GetLink(string id);
    }

    public class LinkRepository(IConnectionMultiplexer redis) : ILinkRepository {
        private readonly IDatabase _database = redis.GetDatabase();

        private static RedisKey BasketKeyPrefix = "Link:";
        private static RedisKey GetRedisKey(string id) => BasketKeyPrefix.Append(id);

        public async Task<bool> CreateTemporaryLink(TemporaryDownloadLink link) {
            var json = JsonSerializer.Serialize(link);
            return await _database.StringSetAsync(GetRedisKey(link.Id), json, link.ExpirationDate - DateTime.Now);
        }

        public async Task<TemporaryDownloadLink?> GetLink(string id) {
            var json = await _database.StringGetAsync(GetRedisKey(id));

            if (json.IsNullOrEmpty)
                return null;

            return JsonSerializer.Deserialize<TemporaryDownloadLink>(json);
        }
    }
}
