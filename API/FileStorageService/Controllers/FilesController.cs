﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using System.Net.Mime;
using Shared.Extensions;
using Shared.DTOs.File;
using MassTransit;
using Shared.Events;
using Shared.Services.FileStorageService;
using FileStorageService.Repositories;

namespace FileStorageService.Controllers {
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    public class FilesController : ControllerBase {
        private readonly ILinkRepository _linkRepository;
        private readonly IFileStorageService _fileService;
        private readonly IPublishEndpoint _publishEndpoint;

        public FilesController(ILinkRepository linkRepository, IFileStorageService fileService, IPublishEndpoint publishEndpoint) {
            _linkRepository = linkRepository;
            _fileService = fileService;
            _publishEndpoint = publishEndpoint;
        }

        /// <summary>
        /// Downloads a file
        /// </summary>
        /// <param name="id">Download id</param>
        [HttpGet("Download/{id}")]
        [ProducesResponseType(typeof(FileContentResult), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(typeof(void), StatusCodes.Status403Forbidden)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(void), StatusCodes.Status500InternalServerError)]
        [Produces(MediaTypeNames.Application.Octet)]
        public async Task<Results<NotFound, FileStreamHttpResult, ForbidHttpResult, ProblemHttpResult, UnauthorizedHttpResult>> DownloadFile(string id) {
            string? UserId = this.GetUserId();

            if (UserId == null)
                return TypedResults.Unauthorized();

            var file = await _linkRepository.GetLink(id);

            if (file == null)
                return TypedResults.NotFound();

            if (file.FileProtection == Shared.Enums.FileProtection.Private && UserId != file.UploadedByUserId)
                return TypedResults.Forbid();

            Stream? requestedFile = await _fileService.Get(file.UploadedByUserId, file.FileId);
            if (requestedFile == null)
                return TypedResults.Problem();

            return TypedResults.File(requestedFile, fileDownloadName: file.FileName);
        }

        /// <summary>
        /// Uploads a file
        /// </summary>
        /// <param name="fileDTO"></param>
        /// <response code="204"></response>
        /// <response code="401">User is not logged in or doesn't have NameIdentifier claim</response>
        /// <response code="500">Server wasn't able to save file</response>
        [HttpPost("Upload")]
        [ProducesResponseType(typeof(void), StatusCodes.Status204NoContent)]
        [ProducesResponseType(typeof(void), StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(typeof(void), StatusCodes.Status500InternalServerError)]
        [Consumes(MediaTypeNames.Multipart.FormData)]
        [Produces(MediaTypeNames.Application.Json)]
        public async Task<Results<UnauthorizedHttpResult, NoContent, ProblemHttpResult>> UploadFile([FromForm] UploadFileDTO fileDTO) {
            string? UserId = this.GetUserId();
            if (UserId == null)
                return TypedResults.Unauthorized();

            string fileId = Guid.NewGuid().ToString();

            bool fileSaved = await _fileService.Save(UserId, fileId, fileDTO.File);
            if (!fileSaved)
                return TypedResults.Problem();

            await _publishEndpoint.Publish(new FileUploadedEvent(UserId, fileId, fileDTO.FolderId, fileDTO.File.FileName, fileDTO.Protection));
            return TypedResults.NoContent();
        }
    }
}
