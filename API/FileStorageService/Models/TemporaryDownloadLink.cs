﻿using Shared.Enums;
using System.ComponentModel.DataAnnotations;

namespace FileStorageService.Models {
    public class TemporaryDownloadLink {
        [Required]
        public string Id { get; set; }
        [Required]
        public string FileId { get; set; }
        [Required]
        public string UploadedByUserId { get; set; }
        [Required]
        public string DownloadedByUserId { get; set; }
        [Required]
        public string FileName { get; set; }
        [Required]
        public FileProtection FileProtection { get; set; }
        [Required]
        public DateTime ExpirationDate { get; set; }

        public TemporaryDownloadLink(string id, string fileId, string fileName, FileProtection fileProtection, DateTime expirationDate, string uploadedByUserId, string downloadedByUserId) {
            Id = id;
            FileName = fileName;
            FileProtection = fileProtection;
            ExpirationDate = expirationDate;
            FileId = fileId;
            DownloadedByUserId = downloadedByUserId;
            UploadedByUserId = uploadedByUserId;
        }
    }
}
