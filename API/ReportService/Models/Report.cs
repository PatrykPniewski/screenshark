﻿using Shared.Enums;
using System.ComponentModel.DataAnnotations;

namespace ReportService.Models {
    public class Report {
        [MaxLength(36), MinLength(36)]
        public string Id { get; set; }
        public string UserId { get; set; }
        public string FileId { get; set; }
        [MaxLength(100)]
        public string? Comment { get; set; }
        public DateTime CreatedAt { get; set; }
        public ReportState State { get; set; }

        public Report(string fileId, string userId, string? comment = null) {
            Id = Guid.NewGuid().ToString();
            FileId = fileId;
            UserId = userId;
            Comment = comment;
            CreatedAt = DateTime.Now;
            State = ReportState.AWAITING;
        }
    }
}
