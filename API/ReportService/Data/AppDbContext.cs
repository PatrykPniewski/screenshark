﻿using Microsoft.EntityFrameworkCore;

namespace ReportService.Data {
    public class AppDbContext : DbContext {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }

        public DbSet<Models.Report> Reports => Set<Models.Report>();
    }
}
