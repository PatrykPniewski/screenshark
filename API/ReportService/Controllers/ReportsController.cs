﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Shared.DTOs.Report;
using ReportService.Data;
using ReportService.Models;
using Microsoft.AspNetCore.Http.HttpResults;
using System.Net.Mime;

namespace ReportService.Controllers {
    /// <summary>
    /// Reports controller
    /// </summary>
    [Route("api/[controller]")]
    [Authorize(Roles = "Admin")]
    [ApiController]
    [Consumes(MediaTypeNames.Application.Json)]
    [Produces(MediaTypeNames.Application.Json)]
    public class ReportsController : ControllerBase {
        private readonly AppDbContext _context;
        private readonly IMapper _mapper;
        private readonly Options.PagingOptions _options;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="mapper"></param>
        /// <param name="options"></param>
        public ReportsController(AppDbContext context, IMapper mapper, IOptions<Options.PagingOptions> options) {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _mapper = mapper;
            _options = options.Value;
        }

        // GET: api/Reports/5
        /// <summary>
        /// Gets list of file ids and report count sorted descending
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        /// <response code="200"></response>
        /// <response code="401">User is not logged in</response>
        /// <response code="403">User doesn't have required role</response>
        [HttpGet("{page:int:min(0)}")]
        [ProducesResponseType(typeof(List<GetReportsOvierviewDTO>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(typeof(void), StatusCodes.Status403Forbidden)]
        public async Task<Results<UnauthorizedHttpResult, ForbidHttpResult, Ok<List<GetReportsOvierviewDTO>>>> GetReportsOverview(int page = 0) {
            return TypedResults.Ok(await _context.Reports
                .GroupBy(report => report.FileId)
                .ProjectTo<GetReportsOvierviewDTO>(_mapper.ConfigurationProvider)
                .OrderByDescending(dto => dto.ReportCount)
                .Skip(_options.ReportsPerPage * page)
                .Take(_options.ReportsPerPage)
                .ToListAsync());
        }

        // GET: api/Reports/File/{fileId}/5
        /// <summary>
        /// Gets list of reports for specific file
        /// </summary>
        /// <param name="fileId"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        /// <response code="200"></response>
        /// <response code="401">User is not logged in</response>
        /// <response code="403">User doesn't have required role</response>
        [HttpGet("File/{fileId}/{page=0:int:min(0)}")]
        [ProducesResponseType(typeof(List<Report>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(typeof(void), StatusCodes.Status403Forbidden)]
        public async Task<Results<UnauthorizedHttpResult, ForbidHttpResult, Ok<List<Report>>>> GetReports(string fileId, int page) {
            return TypedResults.Ok(await _context.Reports
                .Where(report => report.FileId == fileId)
                .Skip(_options.ReportsPerPage * page)
                .Take(_options.ReportsPerPage)
                .ToListAsync());
        }

        // PUT: api/Reports/File/5
        // TODO: Make it possible to accept reports of specific kind rather than all
        /// <summary>
        /// Updates all reports for specific file
        /// </summary>
        /// <param name="fileId"></param>
        /// <param name="updateReportsDTO"></param>
        /// <returns></returns>
        /// <response code="204"></response>
        /// <response code="401">User is not logged in</response>
        /// <response code="403">User doesn't have required role</response>
        [HttpPut("/File/{fileId}")]
        [ProducesResponseType(typeof(void), StatusCodes.Status204NoContent)]
        [ProducesResponseType(typeof(void), StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(typeof(void), StatusCodes.Status403Forbidden)]
        public async Task<Results<UnauthorizedHttpResult, ForbidHttpResult, NoContent>> UpdateReports(string fileId, UpdateReportsDTO updateReportsDTO) {
            await _context.Reports
                .Where(report => report.FileId == fileId)
                .ExecuteUpdateAsync(prop => prop.SetProperty(report => report.State, updateReportsDTO.ReportState));

            return TypedResults.NoContent();
        }
    }
}
