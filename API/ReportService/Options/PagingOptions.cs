﻿namespace ReportService.Options {
    public class PagingOptions {
        public const string SectionName = "Paging";

        public int ReportsPerPage { get; set; } = 10;
    }
}
