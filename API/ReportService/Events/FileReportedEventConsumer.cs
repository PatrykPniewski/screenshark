﻿using MassTransit;
using ReportService.Data;
using Shared.Events;

namespace ReportService.Events {
    public class FileReportedEventConsumer : IConsumer<FileReportedEvent> {
        private AppDbContext _context;

        public FileReportedEventConsumer(AppDbContext context) {
            _context = context;
        }

        async public Task Consume(ConsumeContext<FileReportedEvent> message) {
            var report = message.Message;
            _context.Reports.Add(new Models.Report(report.FileId, report.UserId, report.Comment));
            await _context.SaveChangesAsync();
        }
    }
}
