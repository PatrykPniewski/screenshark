using AutoMapper;
using MassTransit;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using ReportService.Data;
using ReportService.Events;
using ReportService.Models;
using Shared.DTOs.Report;
using Shared.Extensions;
using System.Reflection;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddDbContext<AppDbContext>(options => {
    options.UseSqlServer(builder.Configuration["DB_CONN_STR"] ?? throw new KeyNotFoundException("DB_CONN_STR not found in env/cfg"));
});

builder.Services.AddJwtAuth();

var mapperConfig = new MapperConfiguration(cfg => {
    cfg.CreateMap<IGrouping<string, Report>, GetReportsOvierviewDTO>()
        .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Key))
        .ForMember(dest => dest.ReportCount, opt => opt.MapFrom(src => src.Count()));
});
var mapper = mapperConfig.CreateMapper();

builder.Services.AddMassTransit(bus => {
    bus.UsingRabbitMq((context, cfg) => {
        cfg.Host(new Uri(builder.Configuration["RABBITMQ_CONN_STR"] ?? throw new KeyNotFoundException("RABBITMQ_CONN_STR not found in env/cfg")));
        cfg.ConfigureEndpoints(context);
    });

    bus.AddConsumer<FileReportedEventConsumer>();
});

builder.Services.AddSingleton(mapper);
builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(setup => {
    // Include 'SecurityScheme' to use JWT Authentication
    var jwtSecurityScheme = new OpenApiSecurityScheme {
        BearerFormat = "JWT",
        Name = "JWT Authentication",
        In = ParameterLocation.Header,
        Type = SecuritySchemeType.Http,
        Scheme = JwtBearerDefaults.AuthenticationScheme,
        Description = "JWT Token here",

        Reference = new OpenApiReference {
            Id = JwtBearerDefaults.AuthenticationScheme,
            Type = ReferenceType.SecurityScheme
        }
    };

    setup.AddSecurityDefinition(jwtSecurityScheme.Reference.Id, jwtSecurityScheme);

    setup.AddSecurityRequirement(new OpenApiSecurityRequirement {
        { jwtSecurityScheme, Array.Empty<string>() }
    });

    var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
    var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
    setup.IncludeXmlComments(xmlPath);
});

var app = builder.Build();

using (var serviceScope = app.Services.CreateScope()) {
    var dbContext = serviceScope.ServiceProvider.GetRequiredService<AppDbContext>();
    await dbContext.Database.EnsureCreatedAsync();
}

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment()) {
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
