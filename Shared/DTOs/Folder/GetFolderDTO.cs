﻿namespace Shared.DTOs.Folder {
    public class GetFolderDTO {
        public string Id { get; set; }
        public string Name { get; set; }

        public GetFolderDTO(string id, string name) {
            Id = id;
            Name = name;
        }

        public GetFolderDTO() { }
    }
}
