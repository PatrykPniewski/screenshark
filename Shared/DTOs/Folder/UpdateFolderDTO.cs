﻿using System.ComponentModel.DataAnnotations;

namespace Shared.DTOs.Folder {
    public class UpdateFolderDTO {
        [MaxLength(64)]
        public string Name { get; set; }
    }
}
