﻿using Shared.DTOs.File;

namespace Shared.DTOs.Folder {
    public class GetFolderContentsDTO {
        public IEnumerable<GetFileDTO> Files { get; set; }
        public IEnumerable<GetFolderDTO> Folders { get; set; }

        public GetFolderContentsDTO(IEnumerable<GetFileDTO> files, IEnumerable<GetFolderDTO> folders) {
            Files = files;
            Folders = folders;
        }

        public GetFolderContentsDTO() { }
    }
}
