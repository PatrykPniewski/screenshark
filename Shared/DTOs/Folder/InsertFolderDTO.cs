﻿using System.ComponentModel.DataAnnotations;

namespace Shared.DTOs.Folder {
    public class InsertFolderDTO {
        [MaxLength(64)]
        public string Name { get; set; }
        [MaxLength(36)]
        public string? FolderId { get; set; }

        public InsertFolderDTO(string name, string? folderId) {
            Name = name;
            FolderId = folderId;
        }
    }
}
