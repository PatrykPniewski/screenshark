﻿using System.ComponentModel.DataAnnotations;

namespace Shared.DTOs.Auth {
    public class UserLoginDTO {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }

        public UserLoginDTO(string email, string password) {
            Email = email;
            Password = password;
        }
    }
}
