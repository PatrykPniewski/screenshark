﻿using Shared.Enums;

namespace Shared.DTOs.Auth {
    public class RegisterErrorResponse {
        public IEnumerable<ErrorCode> Errors { get; set; }

        public RegisterErrorResponse(IEnumerable<ErrorCode> errors) {
            Errors = errors;
        }
    }
}
