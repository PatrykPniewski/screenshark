﻿using System.ComponentModel.DataAnnotations;

namespace Shared.DTOs.Auth {
    public class UserRegisterDTO : UserLoginDTO {
        [MinLength(5), MaxLength(64)]
        public string UserName { get; set; }

        public UserRegisterDTO(string userName, string email, string password) : base(email, password) {
            UserName = userName;
        }
    }
}
