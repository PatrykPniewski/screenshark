﻿using Shared.Enums;

namespace Shared.DTOs.File {
    public class UpdateFileDTO {
        public string Name { get; set; }
        public FileProtection Protection { get; set; }

        public UpdateFileDTO(string name, FileProtection protection) {
            Name = name;
            Protection = protection;
        }

        public UpdateFileDTO() { }
    }
}
