﻿using Microsoft.AspNetCore.Http;
using Shared.Enums;

namespace Shared.DTOs.File {
    public class UploadFileDTO {
        public FileProtection Protection { get; set; }
        public IFormFile File { get; set; }
        public string? FolderId { get; set; }

        public UploadFileDTO(FileProtection protection, IFormFile file, string? folderId) {
            Protection = protection;
            File = file;
            FolderId = folderId;
        }
    }
}
