﻿using Shared.Enums;

namespace Shared.DTOs.File {
    public class GetFileDTO {
        public string Id { get; set; }
        public string UserId { get; set; }
        public string Name { get; set; }
        public string Extension { get; set; }
        public FileProtection Protection { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? LastUpdated { get; set; }
        public uint DownloadCount { get; set; }

        public GetFileDTO(string id, string userId, string name, string extension, FileProtection protection, DateTime createdAt, DateTime? lastUpdated, uint downloadCount) {
            Id = id;
            UserId = userId;
            Name = name;
            Extension = extension;
            Protection = protection;
            CreatedAt = createdAt;
            LastUpdated = lastUpdated;
            DownloadCount = downloadCount;
        }
    }
}
