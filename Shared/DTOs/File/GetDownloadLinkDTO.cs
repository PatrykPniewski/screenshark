﻿namespace Shared.DTOs.File {
    public class GetDownloadLinkDTO {
        public string Link { get; set; }
        public DateTime ExpirationDate { get; set; }

        public GetDownloadLinkDTO(string link, DateTime expirationDate) {
            Link = link;
            ExpirationDate = expirationDate;
        }
    }
}
