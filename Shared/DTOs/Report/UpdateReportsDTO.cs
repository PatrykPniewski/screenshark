﻿using Shared.Enums;

namespace Shared.DTOs.Report {
    public class UpdateReportsDTO {
        public ReportState ReportState { get; set; }
    }
}
