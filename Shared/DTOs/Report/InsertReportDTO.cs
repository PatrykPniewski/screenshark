﻿namespace Shared.DTOs.Report {
    public class InsertReportDTO {
        public string? Comment { get; set; }

        public InsertReportDTO(string? comment = null) {
            Comment = comment;
        }
    }
}
