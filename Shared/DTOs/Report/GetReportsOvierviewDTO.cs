﻿namespace Shared.DTOs.Report {
    public class GetReportsOvierviewDTO {
        public string Id { get; set; }
        public int ReportCount { get; set; }
    }
}
