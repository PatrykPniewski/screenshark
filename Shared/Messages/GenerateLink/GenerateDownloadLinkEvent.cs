﻿using Shared.Enums;

namespace Shared.Messages.GenerateLink {
    public class GenerateDownloadLinkEvent {
        public string FileId { get; set; }
        public string RequestedByUserId { get; set; }
        public string OwnerUserId { get; set; }
        public string Name { get; set; }
        public FileProtection FileProtection { get; set; }

        public GenerateDownloadLinkEvent(string fileId, string requestedByUserId, string name, FileProtection fileProtection, string ownerUserId) {
            FileId = fileId;
            Name = name;
            FileProtection = fileProtection;
            RequestedByUserId = requestedByUserId;
            OwnerUserId = ownerUserId;
        }
    }
}
