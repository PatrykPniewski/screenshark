﻿namespace Shared.Messages.GenerateLink {
    public class GenerateDownloadLinkResult {
        public string DownloadId {  get; set; }
        public bool Success { get; set; }
        public DateTime ExpirationDate { get; set; }

        public GenerateDownloadLinkResult(string id, bool success, DateTime expirationDate) {
            DownloadId = id;
            Success = success;
            ExpirationDate = expirationDate;
        }
    }
}
