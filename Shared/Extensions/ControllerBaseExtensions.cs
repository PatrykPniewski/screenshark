﻿using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;

namespace Shared.Extensions {
    public static class ControllerBaseExtensions {
        public static string? GetUserId(this ControllerBase controllerBase) {
            return controllerBase.User?.Claims.FirstOrDefault(x => x != null && x.Type.Equals(ClaimTypes.NameIdentifier), null)?.Value;
        }
    }
}
