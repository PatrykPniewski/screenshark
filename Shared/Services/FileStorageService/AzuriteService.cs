﻿using Azure;
using Azure.Storage.Blobs;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace Shared.Services.FileStorageService {
    public class AzuriteService : IFileStorageService {
        private readonly BlobServiceClient _blobServiceClient;
        private readonly BlobContainerClient _fileContainerClient;
        private readonly ILogger _logger;

        public AzuriteService(BlobServiceClient blobServiceClient, ILogger<AzuriteService> logger) {
            _blobServiceClient = blobServiceClient;
            _fileContainerClient = blobServiceClient.GetBlobContainerClient("users");
            _fileContainerClient.CreateIfNotExists();

            _logger = logger;
        }

        public async Task<bool> Delete(string userId, string fileId) {
            try {
                await _fileContainerClient.DeleteBlobAsync(GetPath(userId, fileId));
            } catch (RequestFailedException e) {
                _logger.LogError(e, "User {userId} requested to delete file {fileId} and it failed", userId, fileId);
                return false;
            }

            return true;
        }

        public async Task<Stream?> Get(string userId, string fileId) {
            string path = GetPath(userId, fileId);
            using MemoryStream stream = new();
            try {
                await _fileContainerClient.GetBlobClient(path).DownloadToAsync(stream);
            } catch (RequestFailedException e) {
                _logger.LogError(e, "User {userId} tried to download {fileId} and it failed", userId, fileId);
                return null;
            }
            return stream;
        }

        public async Task<bool> Save(string userId, string fileId, IFormFile data) {
            string path = GetPath(userId, fileId);
            using MemoryStream stream = new();
            await data.CopyToAsync(stream);
            stream.Position = 0;
            try {
                await _fileContainerClient.UploadBlobAsync(path, stream);
            } catch (RequestFailedException e) {
                _logger.LogError(e, "User {userId} tried to save {fileId} and it failed", userId, fileId);
                return false;
            }

            return true;
        }

        private string GetPath(string userId, string fileId) {
            return $"{userId}/{fileId}";
        }
    }
}
