﻿using Microsoft.AspNetCore.Http;

namespace Shared.Services.FileStorageService {
    public interface IFileStorageService {
        public Task<bool> Save(string userId, string fileId, IFormFile data);
        public Task<Stream?> Get(string userId, string fileId);
        public Task<bool> Delete(string userId, string fileId);
    }
}
