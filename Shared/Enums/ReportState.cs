﻿namespace Shared.Enums {
    public enum ReportState {
        AWAITING = 0,
        ACCEPTED = 1,
        REJECTED = 2
    }
}
