﻿namespace Shared.Enums {
    public enum FileProtection {
        Public,
        RequireLogin,
        Protected,
        Private
    }
}
