﻿namespace Shared.Enums {
    public enum ErrorCode {
        NoError,
        InvalidData,
        InvalidEmail,
        InvalidUserName,
        PasswordRequiresNonAlphanumeric,
        PasswordRequiresUpper,
        PasswordRequiresLower,
        PasswordRequiresDigit,
        PasswordRequiresUniqueChars,
        PasswordTooShort,
        UnknownError,
    }
}
