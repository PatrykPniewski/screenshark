﻿namespace Shared.Events {
    public class FileDeletionRequestedEvent {
        public string UserId { get; set; }
        public string FileId { get; set; }

        public FileDeletionRequestedEvent(string userId, string fileId) {
            UserId = userId;
            FileId = fileId;
        }
    }
}
