﻿using Shared.DTOs.Report;

namespace Shared.Events {
    public class FileReportedEvent : InsertReportDTO {
        public string FileId { get; set; }
        public string UserId { get; set; }

        public FileReportedEvent(string fileId, string userId, string? comment) : base(comment) {
            FileId = fileId;
            UserId = userId;
        }
    }
}
