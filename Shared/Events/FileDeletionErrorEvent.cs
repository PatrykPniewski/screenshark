﻿namespace Shared.Events {
    public class FileDeletionErrorEvent { 
        public string FileId { get; set; }
        public string UserId { get; set; }

        public FileDeletionErrorEvent(string fileId, string userId) {
            FileId = fileId;
            UserId = userId;
        }
    }
}