﻿namespace Shared.Events {
    public class FileDeletedEvent {
        public string FileId { get; set; }

        public FileDeletedEvent(string fileId) {
            FileId = fileId;
        }
    }
}