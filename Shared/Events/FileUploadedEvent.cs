﻿using Shared.Enums;

namespace Shared.Events {
    public class FileUploadedEvent {
        public string UserId { get; set; }
        public string FileId { get; set; }
        public string? FolderId { get; set; }
        public string FileName { get; set; }
        public FileProtection FileProtection { get; set; }

        public FileUploadedEvent(string userId, string fileId, string? folderId, string fileName, FileProtection fileProtection) {
            UserId = userId;
            FileId = fileId;
            FolderId = folderId;
            FileName = fileName;
            FileProtection = fileProtection;
        }
    }
}
